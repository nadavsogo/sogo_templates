<?php
//define('USE_FONTAWSOME', true);
//define('USE_ANIMATION', true);
define('ROOT_PATH', get_stylesheet_directory_uri());
// use this array to add post type that will remove the cpt from the url.
const SOGO_CPT_IGNORE = array('supplier');

//include('lib/functions_template/load_more.php');
function sogo_class_autoload()
{

    $sogo_includes = array();
    $sogo_includes = sogo_main_class_autoload($sogo_includes);
    foreach (glob(dirname(__FILE__) . "/lib/class/*.php") as $filename) {
        $sogo_includes[] = "lib/class/" . basename($filename);
    }
    // parent function.
    sogo_include($sogo_includes);

}

function sogo_child_theme_setup()
{

    add_image_size('gallery-image', 604, 472, true);

    add_image_size('gallery-image-2', 890, 581, true);

    add_image_size('item-image', 261, 297, true);

    add_image_size('team-image', 197, 219, true);

    add_image_size('gallery-image', 496, 323, true);

    add_image_size('article-image', 393, 257, true);

    add_image_size('course-image', 494, 386, true);

    add_image_size('event-image', 760, 496, true);

    add_image_size('map', 950, 700, true);


    // load child text domain
    load_child_theme_textdomain('sogoc', get_stylesheet_directory() . '/languages');

//    register_nav_menus(array(
//        'slitelink_nav' => __('Site Link Footer', 'sogoc')
//    ));
    // load external files
    $sogo_includes = array(
        'lib/widget.php',   // Utility functions
        'lib/post-type-init.php',   // Utility functions

    );
    //parent function.
    sogo_include($sogo_includes);
    sogo_class_autoload();
    add_post_type_support('page', 'excerpt');

    if (function_exists('acf_add_options_page')) {

        acf_add_options_page(array(
            'page_title' => __('Theme General Settings', 'sogoc'),
            'menu_title' => __('Theme Settings', 'sogoc'),
            'menu_slug' => 'theme-general-settings',
            'capability' => 'edit_posts',
            'redirect' => false
        ));
//        acf_add_options_sub_page(array(
//            'page_title' => __('Blog Main Page Settings', 'sogoc'),
//            'menu_title' => __('Blog Main Page Settings', 'sogoc'),
//            'menu_slug' => 'blog-page-settings',
//            'capability' => 'edit_posts',
//            'redirect' => false,
//            'parent_slug' => 'edit.php?post_type=page',
//        ));


// the_field('header_title', 'option');

    }
}

add_action('after_setup_theme', 'sogo_child_theme_setup');


function sogo_child_scripts()
{
    $ver = uniqid();
    if (!is_admin()) {
        wp_enqueue_script('maps', '//maps.googleapis.com/maps/api/js?key=AIzaSyC9RJ9_0-Qsue00D1ZuBAZ8kxDLnFcrHiM', array('jquery'), '', true);
        wp_enqueue_script('jquery-ui-slider');
        add_action('wp_enqueue_scripts', 'sogo_load_owl');
        add_action('wp_enqueue_scripts', 'sogo_load_lightGallery');
        add_action('wp_enqueue_scripts', 'sogo_load_waypoint');

//		add_action( 'wp_enqueue_scripts', 'sogo_load_lightGallery' );
//		add_action( 'wp_enqueue_scripts', 'sogo_load_lightbox' );
//		add_action( 'wp_enqueue_scripts', 'sogo_load_waypoint' );
        add_action('wp_enqueue_scripts', 'sogo_load_slickSlider');
        //     add_action('wp_enqueue_scripts', 'sogo_load_easypiechart');

//        wp_enqueue_script('sogo-modenizer', get_stylesheet_directory_uri() . '/js/nav/modernizr.js', array('jquery'), '', true);
        wp_enqueue_script('sogo-nav-aim', get_stylesheet_directory_uri() . '/js/nav/jquery.menu-aim.js', array('jquery'), '', true);
        //    wp_enqueue_script('sogo-nav-main', get_stylesheet_directory_uri() . '/js/nav/main.js', array('jquery'), '', true);
        wp_enqueue_script('sogo-child-scripts', get_stylesheet_directory_uri() . '/js/scripts.js', array('jquery'), $ver, true);
        wp_enqueue_script('sogo-child-locations', get_stylesheet_directory_uri() . '/js/locations.js', array('jquery'), $ver, true);

        wp_localize_script('sogo-child-scripts', 'sogo', array());

        wp_enqueue_script('jquery-chosen',
            get_stylesheet_directory_uri() . '/js/chosen.jquery.min.js', array('jquery'), $ver, true);
        wp_enqueue_script('jquery-ui-datepicker', get_stylesheet_directory_uri() . '/js/jquery-ui.min.js', array('jquery'), $ver, true);
//        wp_enqueue_script('sogo_template_header_script', get_stylesheet_directory_uri() . '/templates/header/002/header-002.js',   array('jquery'), $ver, true);

        wp_enqueue_style('sogo-style', get_stylesheet_uri(), array(), $ver);
        wp_enqueue_style('google-fonts', 'https://fonts.googleapis.com/css?family=Assistant:400,600,700&amp;subset=hebrew');
        wp_enqueue_style('jquery-ui-datepicker-css', get_stylesheet_directory_uri() . '/css/jquery-ui.min.css');
        wp_enqueue_style('chosen-css', get_stylesheet_directory_uri() . '/css/chosen.min.css');


        wp_enqueue_style('templates-css', get_stylesheet_directory_uri() . '/templates.css');

        include 'templates/template.php';
    }
}




function sogo_enque_facebock_comment_script()
{ ?>

    <div id="fb-root"></div>
    <script>(function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/he_IL/sdk.js#xfbml=1&version=v2.5";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>

<?php }


add_action('wp_enqueue_scripts', 'sogo_child_scripts', 2);

function sogo_extra_class($key)
{
    $val = sogo_meta($key);
    if ($val != -1) {
        echo $val;
    }
}

/**
 * remove editor from pages
 */
function hide_editor()
{

    // Get the Post ID.
    if (isset ($_GET['post'])) {
        $post_id = $_GET['post'];
    } else if (isset ($_POST['post_ID'])) {
        $post_id = $_POST['post_ID'];
    }

    if (!isset ($post_id) || empty ($post_id)) {
        return;
    }

    // Get the name of the Page Template file.
    $template_file = get_post_meta($post_id, '_wp_page_template', true);
    $arr = array('front-page.php');

    if (in_array($template_file, $arr)) { // edit the template name
        remove_post_type_support('page', 'editor');
    }

}

add_action('admin_init', 'hide_editor');


function sogo_single_cpt_template($templates = '')
{
    $single = get_queried_object();

    $template = sogo_meta('template', $single->ID);
    if ('project' == $single->post_type && $template != '') {
        $templates = locate_template($template, false);
    }

    return $templates;
}

add_filter('single_template', 'sogo_single_cpt_template');


function sogo_get_next_posts_link($label = null, $max_page = 0)
{
    global $paged, $wp_query;

    if (!$max_page) {
        $max_page = $wp_query->max_num_pages;
    }

    if (!$paged) {
        $paged = 1;
    }

    $nextpage = intval($paged) + 1;

    if (null === $label) {
        $label = __('Next Page &raquo;');
    }

    if (!is_single() && ($nextpage <= $max_page)) {
        return next_posts($max_page, false);
    }

    return false;
}


function sogo_move_comment_field_to_bottom($fields)
{
    $comment_field = $fields['comment'];
    unset($fields['comment']);
    $fields['comment'] = $comment_field;

    return $fields;
}

add_filter('comment_form_fields', 'sogo_move_comment_field_to_bottom');

function cc_mime_types($mimes)
{
    $mimes['svg'] = 'image/svg+xml';

    return $mimes;
}

add_filter('upload_mimes', 'cc_mime_types');


function my_acf_init()
{

    acf_update_setting('google_api_key', 'AIzaSyCESsV_VCvI8NtKYA10oJ8Be_cWeb5SZVc');
}

add_action('acf/init', 'my_acf_init');


function sogo_header_script()
{
    sogo_print_script('_sogo_header_scripts');
}

add_action('wp_head', 'sogo_header_script');

function sogo_footer_script()
{
    sogo_print_script('_sogo_footer_scripts');
}

add_action('wp_footer', 'sogo_footer_script');

function sogo_print_script($key)
{
    if (function_exists('have_rows')) {
        while (have_rows($key, 'option')): the_row();
            echo PHP_EOL . "<!--    " . get_sub_field('name') . "        -->" . PHP_EOL;
            the_sub_field('scripts', false);
            echo PHP_EOL . "<!--   END -  " . get_sub_field('name') . "        -->" . PHP_EOL;
        endwhile;
    }

}

// Social Links
function sogo_get_social_links()
{
    ?>
    <ul class="row d-flex justify-content-end footer-social">
        <?php while (have_rows('_sogo_social_links', 'option')) : the_row();
            $link = get_sub_field('link');
            $text = get_sub_field('text');
            $icon = get_sub_field('icon');
            ?>
            <li class="ml-4">
                <a target="_blank" href="<?php echo $link; ?>"
                   title="<?php echo $text; ?>"
                   aria-label="<?php _e('Social', 'sogoc') ?>">
                    <span class="icon <?php echo $icon; ?>"></span>
                </a>
            </li>
        <?php endwhile; ?>
    </ul>
    <?php
}


//category level
function sogo_get_first_level_cat($cat)
{
    if ($cat->parent == 0) {
        return $cat;
    } else {
        return sogo_get_first_level_cat(get_term($cat->parent, $cat->taxonomy));
    }
}

function sogo_get_first_level_parent($post)
{

    if ($post->post_parent == 0) {
        return $post->ID;
    } else {
        return sogo_get_first_level_parent(get_post($post->post_parent));
    }
}


function sogo_print_box($class, $image_size)
{
    include('templates/article-box.php');
}

function sogo_print_box_gallery($class, $image_size)
{
    include('templates/article-box-gallery.php');
}

function sogo_title($color, $text)
{
    ?>

    <div class="col-12">
        <div class="section-header">

            <h2 class="section-title-<?php echo $color; ?>"><?php echo $text; ?></h2>

        </div>
    </div>

    <?php
    $color = '';
    $text = '';
}

//print tabs
function sogo_print_tabs($value, $active = '')
{

    $args = array(
        'post_type' => 'project',
        'posts_per_page' => '-1',
        'status' => 'publish',
        'meta_query' => array(
            array(
                'key' => '_sogo_status',
                'value' => $value
            )
        )
    );

    $projects = new WP_Query($args);

    include('templates/content-tabs.php');
    wp_reset_postdata();
}

function sogo_print_project_data()
{

}


/**
 *  enable the serach supplier
 */

add_filter('pre_get_posts', 'supplier_search_filter');
function supplier_search_filter($query)
{

    if (!is_admin() && $query->is_main_query() && is_post_type_archive('supplier')) {
        $taxquery = array();

        if (isset($_GET['flr_supplier_cat']) && $_GET['flr_supplier_cat']) {
            $taxquery [] = array(
                'taxonomy' => 'supplier-cat',
                'field' => 'id',
                'terms' => array($_GET['flr_supplier_cat']),
            );
        }

        if (isset($_GET['flr_supplier_location']) && $_GET['flr_supplier_location']) {
            $taxquery [] = array(
                'taxonomy' => 'supplier-location',
                'field' => 'id',
                'terms' => array($_GET['flr_supplier_location']),
            );

        }

        if (!empty($taxquery)) {
            $query->set('tax_query', $taxquery);
        }

    }
}

/**
 *  ajax get child taxonomy ddl
 */
function sogo_get_ddl()
{
    $tax = $_POST['tax'];
    $term = $_POST['term'];

    ?>

    <div class="input-group position-relative" id="city_ddl">
        <label class="sr-only" for="city"><?php _e('City', 'sogoc'); ?></label>
        <?php $args = array(
            'show_option_all' => __('Select City', 'sogoc'),
            'hide_empty' => 0,
            'selected' => 0,
            'name' => 'flr_supplier_location',
            'class' => 'form-control mb-2 mr-sm-2 mb-sm-0',
            'parent' => $term,
            'tab_index' => 0,
            'hierarchical' => 1,
            'taxonomy' => $tax,
            'value_field' => 'term_id'
        );

        wp_dropdown_categories($args); ?>

        <div class="input-group-btn">
            <i class="fa fa-chevron-down" aria-hidden="true"></i>
        </div>
    </div>

    <?php
    die();
}

add_action('wp_ajax_sogo_get_ddl', 'sogo_get_ddl');
add_action('wp_ajax_nopriv_sogo_get_ddl', 'sogo_get_ddl');


function sogo_create_btn($text, $btn_type = '', $btn_src = '#')
{

    $btn_text = $text;
    $class = $btn_type;
    $src = $btn_src;
//    $color_after_before = $color;
    include('templates/btn.php');
}


/**
 * file_get_contents hack for sogo sandbox
 */
function sogo_file_get_contents($file)
{
    $username = 'dev';
    $password = 'sogodev';

    $context = stream_context_create(array(
        'http' => array(
            'header' => "Authorization: Basic " . base64_encode("$username:$password")
        )
    ));
    return file_get_contents($file, false, $context);
}

/**
 * wp_nav_menu items_wrap
 */
function my_nav_wrap()
{
    $home_url = esc_url(home_url('/'));
    $job_url =  get_field('_sogo_button_jobs', 'sogoc');
    $wrap  = '<nav class="nav header__nav align-items-stretch">';
    $wrap .= '<div class="nav__ul flex-wrap align-items-center align-items-lg-stretch">';
    $wrap .= '<a href="'. $home_url .'" class="nav__logo hidden-lg-up"></a>';
    $wrap .= '<div class="nav__exit-icon"></div>';
    $wrap .= '<ul class="d-flex flex-column flex-lg-row">';
    $wrap .= '%3$s';
    $wrap .= '</ul>';
    $wrap .= '<a class="bclass-button nav__jobs" href="'. $job_url .'">'. __('Jobs','sogoc') .'</a>';
    $wrap .= '<div class="nav__social-bar"></div>';
    $wrap .= '</div>';
    $wrap .= '</nav>';
    // return the result
    return $wrap;
}

/**
 * social bar
 */
function sogo_get_social_bar()
{
    $facebook = get_field('_sogo_facebook','options');
    $youtube = get_field('_sogo_youtube','options');
    $linkedin = get_field('_sogo_linkedin','options');
    $instagram = get_field('_sogo_instagram','options');
}
