<?php
/**
 * Created by PhpStorm.
 * User: oren
 * Date: 06-Mar-17
 * Time: 17:50 PM
 */

namespace sogo;


class widget_import_telaviv {
	private $id = 'widget_import_telaviv';
	private $title = '';
	private $option = 'import_telaviv_last_error';
	private $clean = 'import-telaviv-clean-table';
	private $imports = 'import-telaviv';

	/**
	 * import_products constructor.
	 */
	public function __construct() {
		$this->title = __( "Import Tel Aviv", 'sogoc' );
		add_action( 'admin_init', array( &$this, 'update' ) );
		add_action( 'wp_dashboard_setup', array( &$this, 'widget_box' ) );

	}

	function widget_box() {
		add_meta_box( $this->id, $this->title, array( &$this, 'widget_print' ), 'dashboard', 'side', 'core' );
	}


	function widget_print() {
		?>
        <form action="" method="post" enctype="multipart/form-data">
            <label for="upload-xls"><?php _e( 'Upload File', 'sogoc' ) ?></label>
            <input type="file" id="upload-xla" name="xls">
            <input class="button" type="submit" id="<?php echo $this->imports ?>" name="<?php echo $this->imports ?>"
                   value="Start Import User From Excel">
        </form>


        <div class="results">
			<?php $this->error() ?>
        </div>

		<?php
	}

	function update() {

		if ( isset( $_POST[ $this->imports ] ) && isset( $_FILES['xls'] ) ) {

			$run = readexcel( $_FILES['xls']['tmp_name'] );
			update_option($this->option, $run);
		}

		if ( isset( $_GET[ $this->clean ] ) ) {
			delete_option( $this->option );
			wp_redirect( remove_query_arg( $this->clean   ) );
			exit;
		}

	}

	private function error() {
		?>
        <div class="">
			<?php
			$run = get_option( $this->option );


			if ( ! empty( $run ) ) {
                echo "Total Users: " . $run['total_rows'];
				$error = $run['errors'];
				echo "<table border='1' cellpadding='5' cellspacing='0' style='margin:30px auto'><tr><th>ID</th><th>Comment</th><th>Error</th></tr>";
				foreach ( $error as $item ) {
					echo "<tr>";
					echo "<td>";
					echo $item['CODE_NO'];
					echo "</td>";
					echo "<td>";
					echo $item['comment'];
					echo "</td>";
					echo "<td>";
					echo $item['error'];
					echo "</td>";
					echo "</tr>";
				}
				echo "</table>";
				echo "<a href='" . add_query_arg( array( $this->clean => 'true' ) ) . "'> Clean Table </a>";
			}


			?>
        </div>
		<?php
	}
}

new widget_import_telaviv();