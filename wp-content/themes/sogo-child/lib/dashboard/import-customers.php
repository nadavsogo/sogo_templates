<?php
/**
 * Created by PhpStorm.
 * User: oren
 * Date: 06-Mar-17
 * Time: 17:50 PM
 */

namespace sogo;


class widget_import_customers {
	private $id = 'sogo_print_import_actions_pages';
	private $title = '';

	/**
	 * import_products constructor.
	 */
	public function __construct() {
		$this->title = __( "Import Customers'", 'sogoc' );
		add_action( 'admin_init', array( &$this, 'update' ) );
		add_action( 'wp_dashboard_setup', array( &$this, 'widget_box' ) );

	}

	function widget_box() {
		add_meta_box( $this->id, $this->title, array( &$this, 'widget_print' ), 'dashboard', 'side', 'core' );
	}


	function widget_print() {
		?>
        <form action="" method="post">

            <input class=" button" type="submit" id="import-customers" name="import-customers"
                   value="Start Import Customers">
        </form>
        <div class="results">
			<?php $this->error() ?>
        </div>

		<?php
	}

	function update() {

		if ( isset( $_POST['import-customers'] ) ) {
			$db = new db();
			$db->import_customers();

		}
		if ( isset( $_GET['import-clean-table'] ) ) {
			delete_option( 'import_customers_last_error' );
			wp_redirect( remove_query_arg( 'import-clean-table' ) );
			exit;
		}

	}

	private function error() {
		?>
        <div class="">
			<?php
			$error = get_option( 'import_customers_last_error' );
			if ( ! empty( $error ) ) {

				echo "<table border='1' cellpadding='5' cellspacing='0' style='margin:30px auto'><tr><th>CODE_NO</th><th>Comment</th><th>Error</th></tr>";
				foreach ( $error as $item ) {
					echo "<tr>";
					echo "<td>";
					echo $item['CODE_NO'];
					echo "</td>";
					echo "<td>";
					echo $item['comment'];
					echo "</td>";
					echo "<td>";
					echo $item['error'];
					echo "</td>";
					echo "</tr>";
				}
				echo "</table>";
				echo "<a href='" . add_query_arg( array( 'import-clean-table' => 'true' ) ) . "'> Clean Table </a>";
			}


			?>
        </div>
		<?php
	}
}

new widget_import_customers();