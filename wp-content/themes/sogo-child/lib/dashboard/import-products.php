<?php
/**
 * Created by PhpStorm.
 * User: oren
 * Date: 06-Mar-17
 * Time: 17:50 PM
 */

namespace sogo;


class widget_import_products {
	private $id = 'widget_import_products';
	private $title = '';
	private $option = 'import_products_last_error';
	private $clean = 'import-products-clean-table';
	private $imports = 'import-products';

	/**
	 * import_products constructor.
	 */
	public function __construct() {
		ini_set( 'error_log', WP_CONTENT_DIR . '/import.log' );
		$this->title = __( "Import Products", 'sogoc' );
		//	add_action( 'admin_init', array( &$this, 'update' ) );
		add_action( 'wp_ajax_run_import_products', array( &$this, 'runjs' ) );
		add_action( 'wp_ajax_run_import_single_product', array( &$this, 'run_single_js' ) );
		add_action( 'sogo_import_product_action2', array( &$this, 'run_cron2' ) );
		add_action( 'sogo_import_product_action', array( &$this, 'run_cron' ) );
		add_action( 'wp_dashboard_setup', array( &$this, 'widget_box' ) );

	}

	function widget_box() {
		add_meta_box( $this->id, $this->title, array( &$this, 'widget_print' ), 'dashboard', 'side', 'core' );
	}


	function widget_print() {
//		$db = new db();
//		$db->import_item( 'DE01' );
		//wp_set_object_terms( '149980', 'variable', 'product_type' );
//	    debug(wp_get_object_terms( '149980',   'product_type' ) );
//		wp_set_object_terms( '149980', 'variable', 'product_type' );
//		debug(get_post_meta(149980));
		?>
        <form action="" method="post">
            <input class="button" type="submit" id="runImport" name="<?php echo $this->imports ?>"
                   value="Start Import Products">
            <input type="text" name="sku" value="" id="sku">
        </form>

        <a class="button" href="<?php echo add_query_arg( array( 'show-draf-products' => true ), admin_url() ) ?>"
        ><?php echo esc_attr( __( 'Show Draft Products', 'sogoc' ) ) ?></a>


        <script>
            jQuery(document).ready(function () {
                jQuery('#runImport').click(function (e) {
                    e.preventDefault();
                    jQuery('#import_products_progress').html("Please wait...");
                    if (jQuery('#sku').val() == '') {
                        importProducts();
                    } else {
                        importSingleProduct();
                    }

                });
            });
            function importSingleProduct() {
                jQuery.ajax({
                    url: ajaxurl,
                    data: {
                        'action': 'run_import_single_product',
                        'sku': jQuery('#sku').val()
                    },
                    type: 'POST',
                    success: function (response) {
                        jQuery('#import_products_progress').html(response);
                    },
                    error: function () {

                    }
                });

            }

            function importProducts() {
                jQuery.ajax({
                    url: ajaxurl,
                    data: {
                        'action': 'run_import_products',
                        'sku': jQuery('#import_products_progress').html()
                    },
                    type: 'POST',
                    success: function (response) {
                        jQuery('#import_products_progress').html(response);


                        if (response == 'done') {
                        } else {
                            importProducts();
                        }

                    },
                    error: function () {

                    }
                });

            }


        </script>
		<?php
		if ( isset( $_GET['show-draf-products'] ) ) {
			// show draft products
			$products = get_posts(
				array(
					'post_type'   => 'product',
					'numberposts' => - 1,
					'post_status' => 'pending',
				)
			);
			echo "<table border='1' cellpadding='5' cellspacing='0' style='margin:30px auto'><tr><th>ID</th><th>SKU</th></tr>";
			foreach ( $products as $product ) {
				echo "<tr>";
				echo "<td>";
				echo "<a target='_blank' href='" . get_edit_post_link( $product->ID ) . "'>{$product->post_title}</a>";
				echo "</td>";
				echo "<td>";
				echo get_post_meta( $product->ID, '_sku', true );
				echo "</td>";

				echo "</tr>";
			}
			echo "</table>";

		}
		?>
        <div id="import_products_progress" class="results">
			<?php $this->error() ?>
        </div>


		<?php
	}

	/**
	 * @deprecated
	 */
	function update() {

		echo "running";
		$db = new db();
		if ( isset( $_POST['sku'] ) && ! empty( $_POST['sku'] ) ) {


			$db->import_item( $_POST['sku'] );
			wp_redirect( remove_query_arg( $this->clean ) );
			exit;

		}

		if ( isset( $_POST[ $this->imports ] ) ) {

			$db->start();

		}


		if ( isset( $_GET['run'] ) ) {
			$db->import_items();

		}
		if ( isset( $_GET[ $this->clean ] ) ) {
			delete_option( $this->option );
			wp_redirect( remove_query_arg( $this->clean ) );
			exit;
		}


	}


	// @todo: need to check if ajax - start should not reset the counter.
	public function runjs() {



		$db = new db();
    //  echo "done"; die();

//		$run = get_option( 'sogo_run_import' );
		if (   $_POST['sku']  == 'Please wait...' ) {
			$db->start();
		}

		//if ( isset( $_POST['sku'] ) ) {
		$run = get_option( 'sogo_run_import' );

		$count = 0;

//		Array
//		(
//			[76] => Array
//			(
//				[0] => 107
//            [CODE_NO] => 107
//        )


		if ( $run ) {
			foreach ( $run as $key => $item ) {

				$db->import_item( $item['CODE_NO'] );
				echo $item['CODE_NO'], ", ";
				unset( $run[ $key ] );
				//if(++$count > 1){
				break;
				//}

			}

			update_option( 'sogo_run_import', $run );

			echo "in Queue: " . count( $run ) . " <br/>";
		} else {
			echo "done";
		}

		//	}
		// get the next SKU
		if ( count( $run ) == 1 ) {
			echo 'done';
		}

		die();
	}


	public function run_single_js() {
		$db = new db();
		if ( ! empty( $_POST['sku'] ) ) {
			$db->import_item( $_POST['sku'] );
		}
		echo "done";
		die();

	}

	public function run_cron2() {
		error_log( 'run_cron2: ' . microtime() );
		wp_mail( 'oren@sogo.co.il', 'Cron 2 is running', 'ENDs' );

	}

	public function run_cron() {
		error_log( 'run_cron: ' . microtime() );
		wp_mail( 'oren@sogo.co.il', 'Cron is running', 'ENDs' );
		$db = new db();
		$db->import_items_cron();
	}


	private function error() {
		?>
        <div class="">
			<?php
			$error = get_option( $this->option );
			if ( ! empty( $error ) ) {

				echo "<table border='1' cellpadding='5' cellspacing='0' style='margin:30px auto'><tr><th>CODE_NO</th><th>Comment</th><th>Error</th></tr>";
				foreach ( $error as $item ) {
					echo "<tr>";
					echo "<td>";
					echo $item['CODE_NO'];
					echo "</td>";
					echo "<td>";
					echo $item['comment'];
					echo "</td>";
					echo "<td>";
					echo $item['error'];
					echo "</td>";
					echo "</tr>";
				}
				echo "</table>";
				echo "<a href='" . add_query_arg( array( $this->clean => 'true' ) ) . "'> Clean Table </a>";
			}


			?>
        </div>
		<?php
	}
}

new widget_import_products();