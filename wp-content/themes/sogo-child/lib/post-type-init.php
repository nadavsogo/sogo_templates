<?php

//oren
function sogo_get_post_types()
{
    return array(
        //success stories
//		$return[] = array(
//			'type'         => 'qanda',
//			'label'        => __( 'Question & Answers', 'sogoc' ),
//			'menu_icon'    => null,
//			'hierarchical' => false,
//			'supports'     => array(
//				'title',
//				'editor',
//				'excerpt',
//				'thumbnail',
//			)
//		),
//        $return[] = array(
//            'type'         => 'course',
//            'label'        => __( 'Course', 'sogoc' ),
//            'menu_icon'    => null,
//            'hierarchical' => false,
//            'supports'     => array(
//                'title',
//                'editor',
//                'excerpt',
//                'thumbnail',
//            )
//        ),
        $return[] = array(
            'type' => 'event',
            'label' => __('Event', 'sogoc'),
            'menu_icon' => null,
            'hierarchical' => false,
            'supports' => array(
                'title',
                'editor',
                'excerpt',
                'thumbnail',
            )
        ),
//        $return[] = array(
//            'type'         => 'attendants',
//            'label'        => __( 'attendants', 'sogoc' ),
//            'menu_icon'    => null,
//            'hierarchical' => false,
//            'supports'     => array(
//                'title',
//                'editor',
//                'excerpt',
//                'thumbnail',
//            )
//        ),
        $return[] = array(
            'type' => 'projects',
            'label' => __('Projects', 'sogoc'),
            'menu_icon' => null,
            'hierarchical' => false,
            'supports' => array(
                'title',
                'editor',
                'excerpt',
                'thumbnail',
            )
        ),
//        $return[] = array(
//            'type'         => 'production',
//            'label'        => __( 'Production', 'sogoc' ),
//            'menu_icon'    => null,
//            'hierarchical' => false,
//            'supports'     => array(
//                'title',
//                'editor',
//                'excerpt',
//                'thumbnail',
//            )
//        ),
        $return[] = array(
            'type' => 'testimonials',
            'label' => __('Testimonials', 'sogoc'),
            'menu_icon' => 'dashicons-admin-comments',
            'hierarchical' => false,
            'supports' => array(
                'title',
                'editor',
                'excerpt',
                'thumbnail',
            )
        ),
        $return[] = array(
            'type' => 'services',
            'label' => __('Services', 'sogoc'),
            'menu_icon' => 'dashicons-screenoptions',
            'hierarchical' => false,
            'supports' => array(
                'title',
                'editor',
                'excerpt',
                'thumbnail',
            )
        ),
        $return[] = array(
            'type' => 'video-testimonials',
            'label' => __('Video Tetimonials', 'sogoc'),
            'menu_icon' => 'dashicons-video-alt2',
            'hierarchical' => false,
            'supports' => array(
                'title',
                'editor',
                'excerpt',
                'thumbnail',
            )
        ),
        $return[] = array(
            'type' => 'jobs',
            'label' => __('Jobs', 'sogoc'),
            'menu_icon' => 'dashicons-hammer',
            'hierarchical' => false,
            'supports' => array(
                'title',
                'editor',
                'excerpt',
                'thumbnail',
            )
        )
    );


}

function sogo_get_taxonomies()
{
    return
        array(
            array(
                'label' => __('Services Category', 'sogoc'),
                'tax' => 'services-cat',
                'post_type' => 'services',
                'rewrite' => array(
                    'slug' => 'services-cat',
                    'with_front' => false,
                    'hierarchical' => false,
                ),
            )


        );
}



