<?php
/**
 * Created by PhpStorm.
 * User: oren
 * Date: 06-Apr-17
 * Time: 16:18 PM
 */

namespace sogo;


/**
 * Class WCHooks
 * @package sogo
 */
class wc_hooks {


	/**
	 * WCHooks constructor.
	 */
	public function __construct() {

		//	wp_mail( 'oren@sogo.co.il', 'running' ,'this is the message');
		add_action( 'woocommerce_payment_complete_order_status', array( $this, 'payment_complete' ), 10, 2 );

		//	$this->payment_complete(  );
		add_action( 'woocommerce_init', array( $this, 'wc_init' ) );
	}


	/**
	 * Use to send the order to "oren" db
	 *
	 * @param $order_id
	 *
	 * @return mixed
	 */
	function payment_complete( $var, $order_id ) {

		$vat = 17;
		//wp_mail( 'oren@sogo.co.il', 'woocommerce_new_order: ' . $order_id, 'woocommerce_new_order' );
		$order = new \WC_Order( $order_id );

		$myuser_id      = $order->get_user_id() ; // (int) $order->user_id;
		$total_price     = $order->get_total();
		$user_info      = get_userdata( $myuser_id );
		$user_code      = get_user_meta( $myuser_id, 'CODE_NO', true );
		$items          = $order->get_items();
		$db             = new db();
		$tz             = get_user_meta( $myuser_id, '_sogo_tz', true );
		$number_of_rows = count( $items );
		$current_row    = 1;
		$dbnumber       = get_field('_sogo_next_order_number', 'option') +1;
		update_field('_sogo_next_order_number', $dbnumber, 'option');
		update_post_meta($order_id,'_sogo_external_order_number', $dbnumber );
		foreach ( $items as $item_id => $item ) {


			$item_meta = get_metadata( 'order_item', $item_id );
 			$qty = $item->get_quantity();// $item_meta['_qty'][0];

			//$row_total = $item_meta['_line_total'][0]
			$price = $order->get_item_total( $item )   ;
			$row_price_total = $price * $qty  ;

			$row_price_vat = number_format($row_price_total / 117 *17, 2 )  ;

			//debug( $item_meta['pa_color'][0] );
			$pa_color_code = '';

			$pa_size_code  = '';
			if(isset( $item_meta['pa_size'][0] ) ){
				$size_term = get_term_by('slug',$item_meta['pa_size'][0],'pa_size' ) ;


				if($size_term){
					$pa_size_code = $size_term->name;
				}
			}

			if ( isset( $item_meta['pa_color'][0] ) ) {
				$term = get_term_by( 'slug', $item_meta['pa_color'][0], 'pa_color' );

				if ( $term ) {
					$pa_color_code = get_term_meta( $term->term_id, 'CODE_NO', true );
				}
			}


			try {
				$sku          = get_post_meta( $item_meta['_product_id'][0], '_sku', true );
				$product_name = get_the_title( $item_meta['_product_id'][0] );


				$row = array(
					'MAJOR_REF'     => "'" . $order_id . "'",
					'SERIES_NBR'    => "'2306'",
					'DOC_NBR'       => "'" . $dbnumber  . "'",
					'REF_DATE'      => "TO_DATE('" . date( 'Y-m-d:h:i:s' ) . "','YYYY-MM-DD:hh:mi:ss')",
					'ACCOUNT_NBR'   => "'" . $user_code . "'",
					'ACCOUNT_NAME'  => "'" . $db->convert_to_db( $order->get_billing_first_name() ) . "'",
					'FIN_DETAILS'   => "'" . $tz . "'",
					'NUM_ROWS'      => "'" . $number_of_rows . "'",
					'CONTACT_MAN1'  => "'" . $db->convert_to_db( $order->get_billing_first_name()) . "'",
					'STREET'        => "'" . $db->convert_to_db( $order->get_billing_address_1() . " " . $order->get_billing_address_2() ) . "'",
					'TOWN'          => "'" . $db->convert_to_db( $order->get_billing_city() ) . "'",
					'STREET2'       => "'" . $db->convert_to_db( $order->get_shipping_address_1() . " " . $order->get_shipping_address_2() ) . "'",
					'TOWN2'         => "'" . $db->convert_to_db( $order->get_shipping_city() ) . "'",
					'EMAIL'         => "'" . $user_info->user_email . "'",
					'ROW_NBR'       => "'" . $current_row ++ . "'",
					'ITEM_CODE'     => "'" . $sku . "'",
					'PRICE'         => "'" . $price . "'", // inc vat
					'TOTAL_VALUE'   => "'" . $total_price . "'",// inc vat
					'VAT'           => "'" . $vat . "'", // 17
					'FOR_VAT_VALUE' => "'" . $row_price_vat . "'",  // 17% in shekels
					'TEL1'          => "'" . $order->get_billing_phone() . "'",
					'TEL2'          => "' '",
					'ITEM_NAME'     => "'" . $db->convert_to_db( $product_name ) . "'",
					'ATTRIB1'       => "'" . $pa_color_code . "'",
					'SIZE_DESC'     => "'" . $pa_size_code . "'",
					'QUAN'          => "'" . $qty . "'",
					'ROW_TOTAL'     => "'" . $row_price_total . "'",


				);

			
				ob_start();
				print_r( $row );
				$dump = ob_get_clean();
			//	wp_mail( 'oren@sogo.co.il', 'new order to db', $dump );

				$db->insert_new_order( $row );

			} catch ( Exception $e ) {
				ob_start();
				print_r( $row );
				$dump = ob_get_clean();

				wp_mail( 'oren@sogo.co.il', $e->getMessage(), $dump );

			}


		}

		return $order_id;
	}


	/**
	 *  init special prices
	 */

	public function wc_init() {


//		add_filter( 'woocommerce_product_variation_get_price', array( &$this, 'custom_price_WPA111772' ), 99, 2 );
//		add_filter( 'woocommerce_get_variation_regular_price', array( &$this, 'custom_price_WPA111772' ), 99, 2 );
//		add_filter( 'woocommerce_get_variation_price', array( &$this, 'custom_price_WPA111772' ), 99, 2 );
//		add_filter( 'woocommerce_get_price_html', array( &$this, 'custom_price_WPA111772' ), 99, 2 );
//		add_filter( 'woocommerce_get_variation_price', array(
//			&$this,
//			'filter_woocommerce_get_variation_price'
//		), 10, 4 );

	}


// define the woocommerce_get_variation_price callback
	function filter_woocommerce_get_variation_price( $price, $instance, $min_or_max, $include_taxes ) {
		// make filter magic happen here...
		$cutomer_price = get_post_meta( $instance->get_id(), '_sogo_price2', true );

		if ( ! empty( $cutomer_price ) ) {
			return $cutomer_price;
		}

		return $price;
	}

// add the filter


	function custom_price_WPA111772( $price, $product ) {
		if ( ! is_user_logged_in() ) {
			return $price;
		}

		if ( $product->is_type( 'simple' ) ) {
			return $price;
		}

		$prices = $product->get_variation_prices( true );

		if ( empty( $prices['price'] ) ) {
			return apply_filters( 'woocommerce_variable_empty_price_html', '', $this );
		}


		$new_prices                  = array();
		$new_prices['min_price']     = $product->get_variation_price( 'min', false );
		$new_prices['max_price']     = $product->get_variation_price( 'max', false );
		$new_prices['min_reg_price'] = $product->get_variation_regular_price( 'min', true );
		$new_prices['max_reg_price'] = $product->get_variation_regular_price( 'max', true );

//
		$new_prices = $this->get_variation_tax_status_price( $new_prices, $product );
		extract( $new_prices );

		if ( $min_price !== $max_price ) {
			$price = apply_filters( 'woocommerce_variable_price_html', wc_format_price_range( $min_price, $max_price ) . $product->get_price_suffix(), $product );
		} elseif ( $product->is_on_sale() && $min_reg_price === $max_reg_price ) {
			$price = apply_filters( 'woocommerce_variable_price_html', wc_format_sale_price( wc_price( $max_reg_price ), wc_price( $min_price ) ) . $product->get_price_suffix(), $product );
		} else {
			$price = apply_filters( 'woocommerce_variable_price_html', wc_price( $min_price ) . $product->get_price_suffix(), $product );
		}

		return $price;

	}


	public function get_variation_tax_status_price( $prices, $product ) {
		if ( 'incl' === get_option( 'woocommerce_tax_display_shop' ) ) {
			$prices['min_price']     = '' === $prices['min_price'] ? '' : wc_get_price_including_tax( $product, array(
				'qty'   => 1,
				'price' => $prices['min_price']
			) );
			$prices['max_price']     = '' === $prices['max_price'] ? '' : wc_get_price_including_tax( $product, array(
				'qty'   => 1,
				'price' => $prices['max_price']
			) );
			$prices['min_reg_price'] = '' === $prices['min_reg_price'] ? '' : wc_get_price_including_tax( $product, array(
				'qty'   => 1,
				'price' => $prices['min_reg_price']
			) );
			$prices['max_reg_price'] = '' === $prices['max_reg_price'] ? '' : wc_get_price_including_tax( $product, array(
				'qty'   => 1,
				'price' => $prices['max_reg_price']
			) );
		} else {
			$prices['min_price']     = '' === $prices['min_price'] ? '' : wc_get_price_excluding_tax( $product, array(
				'qty'   => 1,
				'price' => $prices['min_price']
			) );
			$prices['max_price']     = '' === $prices['max_price'] ? '' : wc_get_price_excluding_tax( $product, array(
				'qty'   => 1,
				'price' => $prices['max_price']
			) );
			$prices['min_reg_price'] = '' === $prices['min_reg_price'] ? '' : wc_get_price_excluding_tax( $product, array(
				'qty'   => 1,
				'price' => $prices['min_reg_price']
			) );
			$prices['max_reg_price'] = '' === $prices['max_reg_price'] ? '' : wc_get_price_excluding_tax( $product, array(
				'qty'   => 1,
				'price' => $prices['max_reg_price']
			) );
		}

		return $prices;
	}


}

$sogo_wc_hooks = new wc_hooks();

