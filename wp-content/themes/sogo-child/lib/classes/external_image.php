<?php
/**
 * Created by PhpStorm.
 * User: oren
 * Date: 09-Mar-17
 * Time: 7:45 AM
 */

namespace sogo;


class external_image {

	var $import_path = "";
	var $ext = 'jpg';

	/**
	 * external_image constructor.
	 */
	function __construct() {
		$this->import_path = ABSPATH . '/import-images';
	}

	/**
	 * return file fill path if exists
	 *
	 * @param $sku
	 *
	 * @return bool|string
	 */
	public function file_exists( $sku ) {

		// build file name
		$file_name = $this->import_path . "/" . $sku . '.' . $this->ext;
		if ( \file_exists( $file_name ) ) {
			return $file_name;
		}

		return false;
	}

	public function upload_file( $sku , $post_id = 0) {
		if(!$sku){
			return new \WP_Error('-1', "File not exists");
		}

		$file_full_path = $this->file_exists($sku);

		if ( ! function_exists( 'media_handle_upload' ) ) {
			require_once( ABSPATH . "wp-admin" . '/includes/image.php' );
			require_once( ABSPATH . "wp-admin" . '/includes/file.php' );
			require_once( ABSPATH . "wp-admin" . '/includes/media.php' );
		}
		$desc = '';
		$filename    = basename( $file_full_path );
		$wp_filetype = wp_check_filetype( $filename, null );
		// Array based on $_FILE as seen in PHP file uploads
		$file_array = array(
			'name'     => $filename, // ex: wp-header-logo.png
			'type'     => $wp_filetype['type'],
			'tmp_name' => $file_full_path,
			'error'    => 0,
			'size'     => filesize( $file_full_path ),
		);



		// do the validation and storage stuff
		$id = media_handle_sideload( $file_array, $post_id, $desc );


		@unlink( $file_array['tmp_name'] );
		return $id;


	}


}