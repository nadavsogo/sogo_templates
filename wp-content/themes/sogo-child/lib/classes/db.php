<?php
/**
 * Created by PhpStorm.
 * User: oren
 * Date: 28-Feb-17
 * Time: 9:12 AM
 */

namespace sogo;


class db {

	private $conn = false;
	private $size_tax;
	private $color_tax;
	private $discount = 0.05;
	private $groups = array();
	private $categories = array();
	private $colors = array();

	public $tbl_clients = 'worker17.VIEWCLIENTS';
	public $tbl_colors = 'worker17.VIEWCOLORS';
	public $tbl_groups = 'worker17.VIEWGROUPS';
	public $tbl_items = 'worker17.VIEWITEMS';
	public $tbl_itemsect = 'worker17.VIEWITEMSECT';
	public $tbl_itrot = 'worker17.VIEWITROT';
	public $tbl_prices = 'worker17.VIEWPRICES';
	public $tbl_rulers = 'worker17.VIEWRULERS';
	public $tbl_tarifs = 'worker17.VIEWTARIFS';

	public $tbl_order_status = 'worker17.VIEWCPIDAT';

	// writable tables
	public $tbl_w_clients = 'Workerclients';
	public $tbl_w_orders = 'Workercpi ';

	public $prices = array();


	/**
	 * db constructor.
	 */
	public function __construct() {
		ini_set( 'error_log', WP_CONTENT_DIR . '/import.log' );
		ini_set( "display_errors", "1" );
		error_reporting( E_ALL );
		$this->init();
		$this->fill_group_codes();
		//$this->import_colors();
		$this->fill_colors();
		$this->fill_category_desc();
		$this->size_tax  = wc_attribute_taxonomy_name( 'Size' );
		$this->color_tax = wc_attribute_taxonomy_name( 'Color' );
		$dis             = get_field( '_sogo_discount', 'option' );
		$this->discount  = 1 - $dis;


	}

	/**
	 *  Set the lang to hebrew
	 */
	private function init() {
		putenv( "NLS_LANG=.WE8MSWIN1252" );

	}

	/**
	 * convert CP1252 to UTF-8 Hebrew
	 *
	 * @param $str
	 *
	 * @return string
	 */
	public function convert( $str ) {
		return iconv( 'Windows-1255', 'UTF-8', $str );
	}

	/**
	 * convert CP1252 to UTF-8 Hebrew
	 *
	 * @param $str
	 *
	 * @return string
	 */
	public function convert_to_db( $str ) {
		return iconv( 'UTF-8', 'Windows-1255', $str );
	}

	/**
	 * Run SQL query against Oren DB and return array with the rows
	 *
	 * @param $sql
	 *
	 * @return array
	 */
	public function query( $sql ) {
		$this->open_connection();
		$stid = oci_parse( $this->conn, $sql );
		oci_execute( $stid );
		$rows = array();
		while ( $row = oci_fetch_array( $stid ) ) {
			$rows[] = $row;
		}
		$this->close_connection();

		return $rows;


	}

	/**
	 * open connection to Oren DB
	 * @return WP_Error
	 */
	private function open_connection() {

		try {
			$conn = \oci_connect( 'orenout', 'WrkOut16', '192.116.212.91/orennet.world', 'WE8MSWIN1252' );

			if ( ! $conn ) {
				$m = \oci_error();

				return new WP_Error( 'Error', __( "Error connect to DB", "sogoc" ) );
			} else {
				$this->conn = $conn;
			}
		} catch ( \Exception $error ) {
			var_dump( $error );
		}


	}

	/**
	 * CLose the connection to the Oren DB
	 */
	private function close_connection() {
		// Close the Oracle connection
		\oci_close( $this->conn );
	}

	/**
	 * Will import from Oren DB to WordPress all users, with no update.
	 * @return \WP_Error
	 *
	 */
	public function import_customers() {
		$error = array();
		$rows  = $this->query( 'select * from ' . $this->tbl_clients );

		foreach ( $rows as $row ) {
			// get Oren ID
			// check if the user exist with that id
			$code_id = $row['CODE_NO'];

			$user_id = null;
			$user    = $this->get_user_by_meta( 'CODE_NO', $code_id );

			// we are safe to create the user.
			$local_name = $this->convert( $row['LOCAL_NAME'] );
			$first_name = $last_name = '';
			$userdata   = array(
				'user_url'     => '',
				'user_pass'    => null, // When creating an user, `user_pass` is expected.
				'user_email'   => $row['EMAIL1'],
				'user_login'   => $row['EMAIL1'],
				'first_name'   => $first_name,
				'last_name'    => $last_name,
				'display_name' => $local_name,
				'nickname'     => $local_name,
				'description'  => null,
				'role'         => 'subscriber',
			);
			if ( ! empty( $local_name ) ) {
				$split = explode( ' ', $local_name );
				if ( isset( $split[1] ) ) {
					$first_name = $split[0];
					$last_name  = $split[1];
				}
			}

			if ( ! $user ) {
				// create a user
				if ( email_exists( $row['EMAIL1'] ) ) {
					$error[] = array(
						'CODE_NO' => $code_id,
						'comment' => $row['EMAIL1'],
						'error'   => 'Email already exists in the system',
					);
					continue;
					//return new \WP_Error( 'Error', __( 'Import User: User email already in use', 'sogoc' ) );
				}


			} else {
				// update a user

				$userdata['ID'] = $user->ID;

			}


			$user_id = wp_insert_user( $userdata );

			if ( is_wp_error( $user_id ) ) {
				$error[] = array(
					'CODE_NO' => $code_id,
					'comment' => '',
					'error'   => $user_id->get_error_message(),
				);
				continue;
			}

			// for new and update set the values
			update_user_meta( $user_id, 'CODE_NO', $code_id );
			update_user_meta( $user_id, 'AGENT1', $row['AGENT1'] ); // internet agent = 45
			update_user_meta( $user_id, 'CARD_CODE', $row['CARD_CODE'] );
			update_user_meta( $user_id, 'SORT_CODE', $row['SORT_CODE'] );
			update_user_meta( $user_id, 'TARIF_NBR', $row['TARIF_NBR'] );
			update_user_meta( $user_id, 'CHARACTER5', $row['CHARACTER5'] ); // internet customer =1


			update_user_meta( $user_id, 'EMAIL2', $row['EMAIL2'] );
			update_user_meta( $user_id, 'TEL2', $row['TEL2'] );
			update_user_meta( $user_id, 'FAX1', $row['FAX1'] );

			// shipping info
			update_user_meta( $user_id, 'shipping_first_name', $first_name );
			update_user_meta( $user_id, 'shipping_last_name', $last_name );
			update_user_meta( $user_id, 'shipping_address_1', $this->convert( $row['MAIL_LOCAL_STREET'] ) );
			update_user_meta( $user_id, 'shipping_address_2', $this->convert( $row['LOCAL_SUBURB'] ) );
			update_user_meta( $user_id, 'shipping_city', $this->convert( $row['MAIL_LOCAL_TOWN'] ) );

			update_user_meta( $user_id, 'shipping_postcode', $this->convert( $row['MAIL_ZIPCODE'] ) );
			update_user_meta( $user_id, 'shipping_country', 'IL' );
			update_user_meta( $user_id, 'shipping_state', 'Israel' );


		}
		if ( ! empty( $error ) ) {
			update_option( 'import_customers_last_error', $error );


		}

	}

	public function import_colors() {
		$rows = $this->query( 'select * from ' . $this->tbl_colors );
		foreach ( $rows as $row ) {
			// get Oren ID
			// check if the user exist with that id
			$local_name  = $this->convert( $row['LOCAL_NAME'] );
			$new_term_id = false;
			$term_id     = wp_insert_term(
				$local_name, // the term
				'pa_color', // the taxonomy
				array()
			);


			if ( is_wp_error( $term_id ) ) {
				debug( $term_id );
				$new_term_id = $term_id->error_data['term_exists'];

			} else {
				$new_term_id = $term_id->term_id;
			}
			if ( $new_term_id ) {
				update_term_meta( $new_term_id, 'CODE_NO', $row['CODE_NO'] );
				update_term_meta( $new_term_id, 'ENG_NAME', $row['ENG_NAME'] );
			}


		}
	}

	private function fill_group_codes() {

		$rows = $this->query( "select * from {$this->tbl_groups} " );

		foreach ( $rows as $row ) {
			$this->groups[ $row['CODE_NO'] ] = $this->convert( $row['LOCAL_NAME'] );
		}
	}

	private function fill_category_desc() {

		$rows = $this->query( "select * from {$this->tbl_itemsect} " );

		foreach ( $rows as $row ) {
			$this->categories[ $row['CODE_NO'] ] = $this->convert( $row['LOCAL_NAME'] );
		}
	}

	private function get_colors( $id ) {
		if ( empty( $id ) ) {
			return '';
		}
		$rows = $this->query( "select * from  {$this->tbl_colors} where CODE_NO =  $id" );
		if ( ! empty( $rows ) ) {
			return $this->convert( $rows[0]['LOCAL_NAME'] );
		}
	}


	private function get_cat_desc( $id ) {
		if ( empty( $id ) ) {
			return '';
		}


		if ( isset( $this->categories[ $id ] ) ) {
			return $this->categories[ $id ];
		}

		return '';
	}

	private function get_group( $id ) {
		if ( empty( $id ) ) {
			return '';
		}


		if ( isset( $this->groups[ $id ] ) ) {
			return $this->groups[ $id ];
		}

		return '';
	}


	private function fill_colors() {

		$rows = $this->query( "select * from {$this->tbl_colors} " );

		foreach ( $rows as $row ) {
			$this->colors[ $row['CODE_NO'] ] = $this->convert( $row['LOCAL_NAME'] );
		}
	}

	public function get_colors_array() {
		return $this->colors;
	}

	public function import_prices( $item ) {

		return $this->query( "select * from {$this->tbl_tarifs} where item_code='$item'" );
//		debug($this->prices);

	}

	public function import_stock( $item, $color, $size ) {

		$stocks = $this->query( "select * from {$this->tbl_itrot} where ITEM_NBR='$item' and ITEM_COLOR='$color' and RSIZE='$size' " );
		if ( empty( $stocks ) ) {
			$stocks = $this->query( "select * from {$this->tbl_itrot} where ITEM_NBR='$item'  and RSIZE='$size' and ITEM_COLOR IS NULL " );
		}

		return $stocks;
//		debug($this->prices);

	}

	public function order_status( $order_id  ) {

		$stocks = $this->query( "select * from {$this->tbl_order_status} where MAJOR_REF='$order_id'   " );

		return $stocks;
//		debug($this->prices);

	}

	public function import_item( $item_code ) {
		$start = microtime();

		/**
		 * will hold true if one of the variation has stock, otherwise will be false and the product should not be in
		 * the stock and in the shop so we will move its in stock to false.
		 */
		$is_in_stock = false;
		$is_focus    = false;


		$rows = $this->query( 'select * from ' . $this->tbl_items . " where code_no='" . $item_code . "'" );
		error_log( 'end query to oren: ' . floatval( microtime() - $start ) );
		$index = 0;
		$arr   = array();

		foreach ( $rows as $row ) {
			$index ++;
			if ( ! isset( $arr[ $row['CODE_NO'] ] ) ) {
				$arr[ $row['CODE_NO'] ] = array(
					"name"                 => $this->convert( $row['LOCAL_NAME'] ),
					"slug"                 => $row['ENG_NAME'],
					"sku"                  => $row['CODE_NO'],
					"description"          => $this->convert( $row['LOCAL_MEMO'] ),
					"categories"           => array( $this->get_cat_desc( $row['CHARACTER5'] ) ),
					"sub_category"         => $this->get_cat_desc( $row['CHARACTER2'] ),
					"filter"               => $this->get_cat_desc( $row['CHARACTER1'] ),
					"filter2"              => $this->get_group( $row['GROUP_CODE'] ),
					"audience"             => $this->get_cat_desc( $row['CHARACTER3'] ),
					"brand"                => $this->get_cat_desc( $row['CHARACTER4'] ),
					"focus"                => $this->get_cat_desc( $row['CHARACTER6'] ), // customer's specific product.
					"target"               => $this->get_cat_desc( $row['CHARACTER9'] ),
					"season"               => $this->get_cat_desc( $row['CHARACTER12'] ),
					"formal"               => $this->get_cat_desc( $row['CHARACTER13'] ),
					"sale"                 => $this->get_cat_desc( $row['CHARACTER5'] ),
					"prices"               => $this->import_prices( $row['CODE_NO'] ),
					"mati_iso"             => $row['LOCAL_LOGIC_TEXT3'],
					"european_iso"         => $row['LOCAL_LOGIC_TEXT4'],
					'available_attributes' => array( 'color', "size" ),
					'on_sale'              => intVal( $row['LOGIC9'] ) > 0,

				);

				// need to set all categories and all attributes of products.

				// check if it is focus item - sold only for a specific customer.
				if ( $row['CHARACTER6'] ) {
					$is_focus = true;
				}


			}


			//debug($arr);
			$start = microtime();
			error_log( 'is in stock: ' . $start );
			$stocks = $this->import_stock( $row['CODE_NO'], $row['ITEM_COLOR'], $row['RSIZE'] );


			error_log( 'end in stock: ' . floatval( microtime() - $start ) );

			$itra = 0;
			if ( ! empty( $stocks ) ) {
				$itra = $stocks[0]['ITRA'];

				echo $row['ITEM_COLOR'] . " : " . $itra . "<br/>";
			}

			if ( intval( $itra ) > 0 ) {


				$is_in_stock = true;
				$attributes  = array();

				if ( '' != trim( $row['RSIZE'] ) ) {
					$attributes['size'] = $row['RSIZE'];
				}
				if ( isset( $this->colors[ $row['ITEM_COLOR'] ] ) ) {
					$attributes['color'] = $this->colors[ $row['ITEM_COLOR'] ];
				}
				$arr[ $row['CODE_NO'] ]['variations'][] = array(
					'attributes' => $attributes,
					'price'      => 1,
					'stock'      => $itra,

				);

			}
		}


		/**
		 * if in stock add the product, otherwise will check if the product is in woocommerce and set its stock to 0
		 * and remove all its variations
		 *
		 * if focus add it anyway
		 */


		if ( $is_in_stock || $is_focus ) {
			error_log( 'insert product: ' . $start );
			$this->insert_products( $arr );
			error_log( 'end insert: ' . floatval( microtime() - $start ) );
			if ( $is_focus ) {
				$post_id = $this->get_product_by_meta( '_sku', $item_code );

				if ( $post_id ) {
//					$product = new \WC_Product( $post_id );
//					$product->set_catalog_visibility( 'hidden' );
//					$product->save();
					$post = array( 'ID' => $post_id, 'post_status' => 'private' );
					wp_update_post( $post );
				}
			}

		} else {

			// remove product
			$post_id = $this->get_product_by_meta( '_sku', $item_code );

			if ( $post_id ) {
				$product = new \WC_Product( $post_id );

				$product->set_catalog_visibility( 'hidden' );
				$product->save();
				$post = array( 'ID' => $post_id, 'post_status' => 'pending' );
				wp_update_post( $post );
			}

		}


	}

	public function import_items() {
//		$this->import_item( '577' );
//		die();
		echo "RUN";
		//$rows = $this->query( 'select * from ' . $this->tbl_items );
		$rows = get_option( 'sogo_run_import' );// $this->query( 'select DISTINCT CODE_NO  from' . $this->tbl_items  );

		if ( empty( $rows ) ) {
			wp_redirect( add_query_arg( array( 'done' => intval( $_GET['run'] + 1 ) ) ) );
			exit;
		}

		$count = 1;


		$left = array();

		foreach ( $rows as $key => $row ) {

//			if ( $count ++ > 2 ) {
//				break;
//			}

			echo 'import ' . $row['CODE_NO'];
			$this->import_item( $row['CODE_NO'] );


			$left[] = $key;

		}

		foreach ( $left as $item ) {
			unset( $rows[ $item ] );
		}


		update_option( 'sogo_run_import', $rows );
		$this->delete_transiet();

		wp_redirect( add_query_arg( array( 'run' => intval( $_GET['run'] + 1 ) ) ) );
		exit;


	}

	public function import_items_cron() {
		$rows = get_option( 'sogo_run_import' );

		error_log( 'run_cron: ' . print_r( $rows ) );
		if ( empty( $run ) ) {
			$rows = $this->query( 'select DISTINCT CODE_NO  from ' . $this->tbl_items );
			update_option( 'sogo_run_import', $rows );
		}

		$left = array();

		foreach ( $rows as $key => $row ) {

//			if ( $count ++ > 10 ) {
//				break;
//			}

			echo 'import ' . $row['CODE_NO'];
			$this->import_item( $row['CODE_NO'] );


			$left[] = $key;

		}

		foreach ( $left as $item ) {
			unset( $rows[ $item ] );
		}


		update_option( 'sogo_run_import', $rows );
		$this->delete_transiet();
		wp_mail( 'oren@sogo.co.il', 'end running cron import', 'End' );

		//	wp_redirect( add_query_arg( array( 'run' => intval( $_GET['run'] + 1 ) ) ) );
		exit;


	}

	public function start() {
		$run = get_option( 'sogo_run_import' );

		//	error_log( 'run_cron: ' . print_r( $run ) );
		if ( empty( $run ) ) {
			$rows = $this->query( 'select DISTINCT CODE_NO  from ' . $this->tbl_items );
//			$rows = $this->query( 'select DISTINCT CODE_NO  from ' . $this->tbl_items . ' where rownum <= 10' );
			update_option( 'sogo_run_import', $rows );
		}


	}

	private function delete_transiet() {
		global $wpdb;
		$sql = "DELETE FROM $wpdb->options WHERE 'option_name'    LIKE  '%_transient_%'";

		$wpdb->query( $sql );
	}


	/**
	 * Get user by Meta key - will return only first user.
	 *
	 * @param $meta_key
	 * @param $meta_value
	 *
	 * @return mixed
	 */
	private function get_user_by_meta( $meta_key, $meta_value ) {

		$users = get_users(
			array(
				'meta_key'    => $meta_key,
				'meta_value'  => $meta_value,
				'number'      => 1,
				'count_total' => false
			)
		);
		$user  = reset( $users );


		return $user;
	}


	private function get_product_by_meta( $meta_key, $meta_value ) {
		global $wpdb;
		$sql = "SELECT post_id FROM $wpdb->postmeta WHERE meta_key ='$meta_key' AND meta_value = '$meta_value' ";


		return $wpdb->get_var( $sql );

	}

	private function get_terms_by_meta( $taxonomy, $meta_key, $meta_value ) {
		$user = reset(
			get_terms(
				array(
					'taxonomy'   => $taxonomy,
					'hide_empty' => false,
					'meta_query' => array(
						array(
							'key'   => $meta_key,
							'value' => $meta_value,
						)
					)
				)
			)
		);

		return $user;
	}

	private function insert_product( $product_data ) {
		/**
		 *  Product check by code_no
		 */
		$post_id = $this->get_product_by_meta( '_sku', $product_data['sku'] );

		/**
		 *  Set the post thumbnail
		 */

		$file = new external_image();


		$post = array( // Set up the basic post data to insert for our product
			'ID'           => $post_id,
			'post_author'  => 1,
			'post_content' => $product_data['description'],
			'post_status'  => 'publish',
			'post_title'   => $product_data['name'],
			'post_name'    => $product_data['slug'],
			'post_parent'  => '',
			'post_type'    => 'product'
		);

		$post_id = wp_insert_post( $post ); // Insert the post returning the new post id

		if ( ! $post_id ) // If there is no post id something has gone wrong so don't proceed
		{
			return false;
		}

		update_post_meta( $post_id, '_sku', $product_data['sku'] ); // Set its SKU
		update_post_meta( $post_id, '_visibility', 'visible' ); // Set the product to visible, if not it won't show on the front end
		update_post_meta( $post_id, '_sale_price_dates_from', '' );
		update_post_meta( $post_id, '_sale_price_dates_to', '' );
		update_post_meta( $post_id, '_stock_status', 'instock' );
		update_post_meta( $post_id, '_weight', '0' );
		update_post_meta( $post_id, '_height', '0' );
		update_post_meta( $post_id, '_width', '0' );
		update_post_meta( $post_id, '_length', '0' );
		update_post_meta( $post_id, '_tax_status', 'taxable' );
		update_post_meta( $post_id, '_tax_class', '' );
		update_post_meta( $post_id, '_featured', 'no' );
		update_post_meta( $post_id, '_downloadable', 'no' );
		update_post_meta( $post_id, '_virtual', 'no' );
		update_post_meta( $post_id, '_downloadable', 'no' );
		update_post_meta( $post_id, '_sold_individually', '' );
		update_post_meta( $post_id, '_product_attributes', array() );
		update_post_meta( $post_id, '_manage_stock', 'no' );
		update_post_meta( $post_id, '_backorders', 'no' );
		update_post_meta( $post_id, '_stock', '' );
		update_post_meta( $post_id, '_purchase_note', '' );
		update_post_meta( $post_id, '_regular_price', '' );
		update_post_meta( $post_id, '_sale_price', '' );


		/**
		 *  Price group 3 is the price for normal customer and we will use it for the site
		 *  from this price we will reduce the tax and set it to the regular price
		 *  all product on the site have a sale price - set it as well,
		 *  sell_price = option parameter for the % to reduce form the regular price.
		 *
		 *  Formula to extract the VAT: price * 17 / 117 = price with out VAT
		 */
		$prices     = $product_data['prices'];
		$site_price = false;
		if ( ! empty( $prices ) ) {
			foreach ( $prices as $price ) {

				// get tarif 3 first
				if ( 3 == $price['TARIF_NBR'] ) {

					if ( $price['PRICE'] ) {

						// update he sell price if the regular price is not 0
						$site_price = $price['PRICE'];//round( ( $price['PRICE'] / 1.17 ) );
						update_post_meta( $post_id, '_regular_price', $site_price );


						// have 5% discount price for all site items.
						update_post_meta( $post_id, '_sale_price', $site_price * $this->discount );
						update_post_meta( $post_id, '_price', $site_price * $this->discount );
					}

				} else {
					if ( $price['PRICE'] ) {
						// all other prices are with no tax and have no discount on site.
						update_post_meta( $post_id, '_sogo_price' . $price['TARIF_NBR'], $price['PRICE'] );
						if ( ! $site_price ) {
							// update he sell price if the regular price is not 0
							$site_price = $price['PRICE'];
							update_post_meta( $post_id, '_regular_price', $site_price );


							// have 5% discount price for all site items.
							update_post_meta( $post_id, '_sale_price', $site_price );
							update_post_meta( $post_id, '_price', $site_price );
						}

					}
				}


			}
		}

		wp_set_object_terms( $post_id, $product_data['categories'], 'product_cat' ); // Set up its categories

		/**
		 *  always use both of the filters for the main product filter tax
		 *  if the product is formal - as it as ייצוגי
		 */

		if ( $product_data['filter'] == 'ביגוד ייצוגי' ) {
			$product_data['filter'] = 'ביגוד מקצועי';
		}
		if ( $product_data['filter2'] == 'ביגוד' ) {
			$product_data['filter2'] = 'ביגוד מקצועי';
		}
		$taxs = array(
			$product_data['filter'],
			$product_data['filter2']
		);

		if ( $product_data['on_sale']   ) {
			$taxs[] = 'מבצעים';
		}

		if ( $product_data['formal'] == 'כן' ) {
			$taxs[] = 'ביגוד ייצוגי';
		}
		$taxs = array_unique( $taxs );

		/**
		 *  Set all other parameters for the product.
		 *
		 *  - sub_category  = taxonomy
		 *  - filter        = taxonomy
		 *  - audience      = taxonomy
		 *  - brand         = taxonomy
		 *  - focus         = Belong to specific customer and should not be visible to other customers.
		 *  - target        = taxonomy
		 *  - season        = taxonomy
		 *  - formal        = Boolean
		 */
		wp_set_object_terms( $post_id, $product_data['sub_category'], 'product_sub_cat' );
		wp_set_object_terms( $post_id, $taxs, 'product_filter' );
		//wp_set_object_terms( $post_id, $product_data['filter2'], 'product_filter_type' );
		wp_set_object_terms( $post_id, $product_data['audience'], 'product_audience' );
		wp_set_object_terms( $post_id, $product_data['brand'], 'product_brand' );
		wp_set_object_terms( $post_id, $product_data['target'], 'product_target' );
		wp_set_object_terms( $post_id, $product_data['season'], 'product_season' );


		/**
		 *  Set the post thumbnail
		 */

		$file = new external_image();

		$thumb_id = $file->upload_file( $product_data['sku'], $post_id );
		if ( ! is_wp_error( $thumb_id ) ) {
			// if we have upload image now, first delete all other images related to that product
			sogo_delete_post_images( $post_id, $thumb_id );
			set_post_thumbnail( $post_id, $thumb_id );
		}
		$product = new \WC_Product( $post_id );

		if ( $product_data['focus'] ) {
			$post = array( 'ID' => $post_id, 'post_status' => 'private' );
			wp_update_post( $post );
//			$product->set_catalog_visibility( 'hidden' );
//			$product->save();
		} elseif ( has_post_thumbnail( $post_id ) ) {

			//$post = array( 'ID' => $post_id, 'post_status' => 'publish', 'attribute_visibility' => array( 1 ) );
			//wp_update_post( $post );
			$product->set_catalog_visibility( 'hidden' );
			$product->save();
			$product->set_catalog_visibility( 'visible' );
			$product->save();

			//	update_post_meta( $post_id, '_visibility', 'catalog' );
			//	 print_r(get_post_meta($post_id));
		} else {
			$product->set_catalog_visibility( 'hidden' );
			$product->save();
			$post = array( 'ID' => $post_id, 'post_status' => 'pending' );
			//	update_post_meta( $post_id, '_visibility', 'hidden' );
			wp_update_post( $post );

		}

		/**
		 *  Start importing the gallery images per product.
		 */
		$gallery = array();
		for ( $i = 1; $i < 50; $i ++ ) {
			$image_id = $file->upload_file( $product_data['sku'] . "_$i", $post_id );
			if ( ! is_wp_error( $image_id ) ) {
				$gallery[] = $image_id;
			} else {
				break;
			}
		}

		if ( ! empty( $gallery ) ) {
			update_post_meta( $post_id, '_product_image_gallery', implode( ',', $gallery ) );
		}

		if ( isset( $product_data['variations'] ) && ! empty( $product_data['variations'] ) ) {
			if ( ! empty( array_filter( $product_data['variations'][0]['attributes'] ) ) ) {

				wp_set_object_terms( $post_id, 'variable', 'product_type' ); // Set it to a variable product type

				$this->insert_product_attributes( $post_id, $product_data['available_attributes'], $product_data['variations'] ); // Add attributes passing the new post id, attributes & variations
				$this->insert_product_variations( $post_id, $product_data['variations'] ); // Insert variations passing the new post id & variations
				// Update parent if variable so price sorting works and stays in sync with the cheapest child

			}


		} else {

		}


		wc_delete_product_transients( $product->get_id() );
		wp_cache_delete( 'product-' . $product->get_id(), 'products' );


		\WC_Product_Variable::sync( $post_id );


	}

	private function insert_product_attributes( $post_id, $available_attributes, $variations ) {


		foreach ( $available_attributes as $attribute ) // Go through each attribute
		{
			$values = array(); // Set up an array to store the current attributes values.

			foreach ( $variations as $variation ) // Loop each variation in the file
			{
				$attribute_keys = array_keys( $variation['attributes'] ); // Get the keys for the current variations attributes

				foreach ( $attribute_keys as $key ) // Loop through each key
				{
					if ( $key === $attribute ) // If this attributes key is the top level attribute add the value to the $values array
					{
						$values[] = $variation['attributes'][ $key ];
					}
				}
			}

			// Essentially we want to end up with something like this for each attribute:
			// $values would contain: array('small', 'medium', 'medium', 'large');

			$values = array_unique( $values ); // Filter out duplicate values

			// Store the values to the attribute on the new post, for example without variables:
			// wp_set_object_terms(23, array('small', 'medium', 'large'), 'pa_size');

			if ( count( $values ) > 0 ) {
				wp_set_object_terms( $post_id, $values, 'pa_' . $attribute );
			} else {
				echo 'remove';
				wp_delete_object_term_relationships( $post_id, 'pa_' . $attribute );
			}

		}

		$product_attributes_data = array(); // Setup array to hold our product attributes data

		foreach ( $available_attributes as $attribute ) // Loop round each attribute
		{
			// will add attribute only if variation exist
			if ( isset( $variations[0]['attributes'][ $attribute ] ) ) {

				$product_attributes_data[ 'pa_' . $attribute ] = array( // Set this attributes array to a key to using the prefix 'pa'

					'name'         => 'pa_' . $attribute,
					'value'        => '',
					'is_visible'   => '1',
					'is_variation' => '1',
					'is_taxonomy'  => '1'

				);
			}

		}

		update_post_meta( $post_id, '_product_attributes', $product_attributes_data ); // Attach the above array to the new posts meta data key '_product_attributes'
	}

	private function insert_product_variations( $post_id, $variations ) {

		$price      = get_post_meta( $post_id, '_regular_price', true );
		$sale_price = get_post_meta( $post_id, '_sale_price', true );

		// get product variation
		$old_variations = new \WC_Product_Variable( $post_id );
		$old_variations = $old_variations->get_available_variations();

		foreach ( $old_variations as $old_variation ) {
			wp_delete_post( $old_variation['variation_id'], true );
		}
		// delete it all
//		wp_delete_post( $variation_id );
		foreach ( $variations as $index => $variation ) {

			$variation_post = array( // Setup the post data for the variation

				'post_title'  => 'Variation #' . $index . ' of ' . count( $variations ) . ' for product#' . $post_id,
				'post_name'   => 'product-' . $post_id . '-variation-' . $index,
				'post_status' => 'publish',
				'post_parent' => $post_id,
				'post_type'   => 'product_variation',
			);

			$variation_post_id = wp_insert_post( $variation_post ); // Insert the variation


			// Loop through the variations attributes
			foreach ( $variation['attributes'] as $attribute => $value ) {

				//skip if empty
				if ( empty( $value ) ) {

					continue;
				};

				$attribute_term = get_term_by( 'name', $value, 'pa_' . $attribute ); // We need to insert the slug not the name into the variation post meta

				if ( $attribute_term ) {
					update_post_meta( $variation_post_id, 'attribute_pa_' . $attribute, $attribute_term->slug );
				}
				// Again without variables: update_post_meta(25, 'attribute_pa_size', 'small')

			}

			update_post_meta( $variation_post_id, '_sku', '' );
			update_post_meta( $variation_post_id, '_sale_price_dates_from', '' );
			update_post_meta( $variation_post_id, '_sale_price_dates_to', '' );
			update_post_meta( $variation_post_id, '_virtual', 'no' );
			update_post_meta( $variation_post_id, '_downloadable', 'no' );
			update_post_meta( $variation_post_id, '_variation_description', '' );
			update_post_meta( $variation_post_id, '_stock', $variation['stock'] );
			update_post_meta( $variation_post_id, '_stock_status', 'instock' );
			update_post_meta( $variation_post_id, '_manage_stock', 'no' );


			update_post_meta( $variation_post_id, '_regular_price', $price );
			update_post_meta( $variation_post_id, '_price', $sale_price );
			update_post_meta( $variation_post_id, '_sale_price', $sale_price );

		}
	}

	private function insert_products( $products ) {
		if ( ! empty( $products ) ) // No point proceeding if there are no products
		{
			array_map( array(
				&$this,
				'insert_product'
			), $products ); // Run 'insert_product' function from above for each product
		}
	}

	public function sku_sizes_array( $sku ) {
		$arr = $this->query( "select RSIZE from {$this->tbl_items} where CODE_NO='$sku' " );


		$return = array();
		foreach ( $arr as $item ) {
			$return[ $item[0] ] = $item[0];
		}

		return $return;

	}


	/**
	 *  Insert new Order
	 */


	public function insert_new_order( $args ) {

		$defaults = array(
			'MAJOR_REF'      => "'1'",
			'SERIES_NBR'     => "'2305'",
			'DOC_NBR'        => "'123456784'",
			'REF_DATE'       => "TO_DATE('2017-04-07:12:12:01','YYYY-MM-DD:hh:mi:ss')", // yyyy/mm/dd:hh:mi:ss
			'ACCOUNT_NBR'    => "'202112'",
			'ACCOUNT_NAME'   => "''",
			'REF1'           => "''",
			'REF2'           => "''",
			'REF3'           => "''",
			'FIN_DETAILS'    => "''", // tz
			'TRAN_STORE'     => "'12'",
			'NUM_ROWS'       => "''",
			'DISCOUNT1'      => "''",
			'VAT'            => "''",
			'TOTAL_VALUE'    => "''",
			'FOR_VAT_VALUE'  => "''",
			'TOP_MEMO'       => "''",
			'BOTTOM_MEMO'    => "''",
			'CONTACT_MAN1'   => "''",
			'TEL1'           => "''",
			'CONTACT_MAN2'   => "''",
			'TEL2'           => "''",
			'STREET'         => "''",
			'TOWN'           => "''",
			'FAX'            => "''",
			'EMAIL'          => "''",
			'STREET2'        => "''",
			'TOWN2'          => "''",
			'FREE_TEXT1'     => "''",
			'FREE_TEXT2'     => "''",
			'FREE_TEXT3'     => "''",
			'FREE_TEXT4'     => "''",
			'FREE_TEXT5'     => "''",
			'OSEK'           => "''",
			'ROW_NBR'        => "'1'",// row number
			'ITEM_CODE'      => "''",
			'ITEM_NAME'      => "''",
			'ATTRIB1'        => "''",
			'SIZE_DESC'      => "''",
			'PRICE'          => "''",
			'PER_OFF'        => "''",
			'QUAN'           => "''",
			'ROW_TOTAL'      => "''",
			'CLIENT_BARCODE' => "''",
			'ROW_REF'        => "''",
			'OUT_DATE'       => "''",
		);
		$args     = wp_parse_args( $args, $defaults );

		$keys = array_keys( $args );


		$sql = "insert into " . $this->tbl_w_orders . " values (" . implode( ',', $args ) . ")";

		ob_start();
		echo $sql;
		$dump = ob_get_clean();
		//	wp_mail( 'oren@sogo.co.il', 'new order to db', $dump );
		$this->open_connection();
		$stid    = oci_parse( $this->conn, $sql );
		$success = oci_execute( $stid );
		$this->close_connection();

		return $success;

	}


	public function is_customer_exists( $email ) {
		$rows = $this->query( "select * from " . $this->tbl_clients . " where email1='" . $email . "'" );
		if ( $rows ) {
			return $rows[0];
		}

		return false;
	}

	public function insert_customer_crm( $args ) {
		// create the array
		$current_number   = get_field( '_sogo_next_customer_number', 'option' );
		$new_user_code_no = $current_number;
		$defaults         = array(
			'CODE_NO'           => "'" . $current_number . "'",
			'LOCAL_NAME'        => "''",
			'ENG_NAME'          => "''",
			'CARD_CODE'         => "2",
			'SORT_CODE'         => "210",
			'TARIF_NBR'         => "3",
			'CHARACTER1'        => "''",
			'CHARACTER2'        => "''",
			'CHARACTER3'        => "''",
			'CHARACTER4'        => "''",
			'CHARACTER5'        => "''",
			'CHARACTER6'        => "''",
			'CHARACTER7'        => "''",
			'CHARACTER8'        => "''",
			'CHARACTER9'        => "''",
			'CHARACTER10'       => "''",
			'FREE1'             => "''",
			'FREE2'             => "''",
			'FREE3'             => "''",
			'FREE4'             => "''",
			'FREE5'             => "''",
			'FREE6'             => "''",
			'FREE7'             => "''",
			'FREE8'             => "''",
			'FREE9'             => "''",
			'FREE10'            => "''",
			'FREE11'            => "''",
			'FREE12'            => "''",
			'LOCAL_STREET'      => "''",
			'ENG_STREET'        => "''",
			'MAIL_LOCAL_STREET' => "''",
			'MAIL_ENG_STREET'   => "''",
			'LOCAL_SUBURB'      => "''",
			'ENG_SUBURB'        => "''",
			'MAIL_LOCAL_SUBURB' => "''",
			'MAIL_ENG_SUBURB'   => "''",
			'LOCAL_TOWN'        => "''",
			'ENG_TOWN'          => "''",
			'MAIL_LOCAL_TOWN'   => "''",
			'MAIL_ENG_TOWN'     => "''",
			'LOCAL_STATE'       => "''",
			'ENG_STATE'         => "''",
			'MAIL_LOCAL_STATE'  => "''",
			'MAIL_ENG_STATE'    => "''",
			'ZIPCODE'           => "''",
			'MAIL_ZIPCODE'      => "''",
			'LOCAL_OCCUPATION'  => "''",
			'ENG_OCCUPATION'    => "''",
			'TEL1'              => "''",
			'TEL2'              => "''",
			'TEL3'              => "''",
			'TEL4'              => "''",
			'TEL5'              => "''",
			'FAX1'              => "''",
			'FAX2'              => "''",
			'EMAIL1'            => "''",
			'EMAIL2'            => "''",
			'AGENT1'            => "45",
			'AGENT2'            => "''",
			'AGENT3'            => "''",
		);
		$args             = wp_parse_args( $args, $defaults );
		//	$keys     = array_keys( $args );


		$sql = "insert into " . $this->tbl_w_clients . " values (" . implode( ',', $args ) . ")";

		$this->open_connection();
		$stid    = oci_parse( $this->conn, $sql );
		$success = oci_execute( $stid );
		$this->close_connection();
		update_field( '_sogo_next_customer_number', ++ $current_number, 'option' );

		return $new_user_code_no;


	}


//$json = file_get_contents('my - product - data . json'); // Get json from sample file
//$products_data = json_decode($json_file, true); // Decode it into an array
//
//insert_products($products_data)
}