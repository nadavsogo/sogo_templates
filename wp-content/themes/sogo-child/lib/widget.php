<?php
/**
 * Register sidebars
 */
    function sogo_widgets_init()
    {
	    register_sidebar(array(
		    'name' => __('Post right', 'sogoc'),
		    'id' => 'post_right_sidebar',
		    'before_widget' => '<aside class="widget">',
		    'after_widget' => '</aside>',
		    'before_title' => '<h3 class="widget-title">',
		    'after_title' => '</h3>',
	    ));

        register_sidebar(array(
            'name' => __('Blog', 'sogoc'),
            'id' => 'blog_sidebar',
            'before_widget' => '<aside class="sidebar mb-3 %1$s %2$s">',
            'after_widget' => '</aside>',
            'before_title' => '<div class="title-wrapper mb-3 ">
    <div class="row">
        <div class="col">
            <h2 class="widget-title  ">
               
            ',
            'after_title' => '</h2>
		 
        </div>
    </div>
</div>',
        ));


    }
add_action('widgets_init', 'sogo_widgets_init');

