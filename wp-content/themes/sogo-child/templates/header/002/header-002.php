<header class="container header002">

    <div class="row justify-content-between justify-content-lg-unset align-items-lg-stretch py-2 py-lg-0 px-3">


        <!-- HAMBURGER -->
        <div class="col-auto px-0 d-flex align-items-center hidden-lg-up mobile-menu">
            <div class="mobile-menu__strokes">
                <div class="mobile-menu__stroke"></div>
                <div class="mobile-menu__stroke"></div>
                <div class="mobile-menu__stroke"></div>
            </div>
        </div>

        <!-- LOGO -->
        <div class="col-auto col-lg-3 py-lg-2 d-flex px-0 px-lg-3">
            <a class="logo d-flex align-items-center"
               href="<?php echo esc_url( home_url( '/' ) ); ?>"
               title="<?php echo __( 'Go to: homepage', 'sogoc' ) ?>" rel="home">
                <img class="img-fluid logo-img ml-lg-3"
                     src="<?php echo
				     get_field( '_sogo_header002_logo_img', 'options' ); ?>"
                     alt="Logo">
                <!--                <img src="-->
				<?php //echo get_stylesheet_directory_uri() . '/images/slogan.png'?><!--" alt="slogan"-->
                <!--                     class="hidden-md-down slogan">-->
                <div class="slogan hidden-md-down"></div>

            </a>
        </div>

        <!-- MENU -->
        <div class="col-auto header002__menu-container">
            <div class="header002__menu d-flex h-100">
				<?php include 'inc/header-walker-002.php'; ?>
				<?php include 'inc/header-menu-002.php'; ?>
            </div>
        </div>

        <div class="col-lg-3 d-flex align-items-center justify-content-around hidden-md-down">

            <!-- SEARCH -->
            <form role="search" method="get" id="searchform"
                  class="searchform hidden-md-down align-items-center" action="http://localhost/willifood/">
                <div>
                    <label class="screen-reader-text" for="s">חפש:</label>
                    <input type="text" value="" name="s" id="s">
                    <!--                <input type="submit" id="searchsubmit" value="חפש">-->
                    <button type="submit" id="searchsubmit">
						<?php echo sogo_file_get_contents( get_stylesheet_directory_uri() . '/images/icons/search.svg' ); ?>
                    </button>
                </div>
            </form>

            <!-- BUTTON -->
			<?php if ( get_field( '_sogo_header002_button', 'option' ) ) : ?>

                <div class="header002__button hidden-md-down">
                    <a class="button "
                       href="<?php echo get_field( '_sogo_header002_button_link', 'options' ); ?>">
						<?php echo get_field( '_sogo_header002_button', 'options' ) ?>
                    </a>
                </div>

			<?php endif; ?>

            <!-- SOCIAL BAR -->
			<?php if ( have_rows( '_sogo_header002_socialbar', 'options' ) ): ?>
                <div class="col-auto hidden-md-down">
                    <div class="header002__social-bar">
						<?php include "inc/header-social-bar-002.php"; ?>
                    </div>
                </div>
			<?php endif; ?>

            <!-- MULTILINGUAL -->
            <a href="#" class="multilingual hidden-md-down">EN</a>

        </div>

    </div>
</header>