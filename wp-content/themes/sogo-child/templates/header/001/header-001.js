$jQuery(document).ready(function () {
    $('.header001 .mobile-menu').click(function () {
        $('.nav__ul').addClass('nav__ul_show_true');
        $('.nav__exit-icon').addClass('nav__exit-icon_show_true');
    });
    $('.header001 .nav__exit-icon').click(function () {
        $('.nav__ul').removeClass('nav__ul_show_true');
        $('.nav__exit-icon').removeClass('nav__exit-icon_show_true');
    });
});