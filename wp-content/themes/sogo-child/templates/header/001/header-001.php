<header class="container header001">

    <div class="row justify-content-between justify-content-lg-around align-items-lg-stretch
    py-2 py-lg-0 px-3">

        <div class="col-auto d-flex align-items-center hidden-lg-up mobile-menu">
            <div class="mobile-menu__strokes">
                <div class="mobile-menu__stroke"></div>
                <div class="mobile-menu__stroke"></div>
                <div class="mobile-menu__stroke"></div>
            </div>
        </div>

        <div class="col-auto flex-last flex-lg-unordered py-lg-2">
            <a class="logo" href="<?php echo esc_url(home_url('/')); ?>"
               title="<?php echo __('Go to: homepage', 'sogoc') ?>" rel="home">
                <img class="img-fluid logo-img"
                     src="<?php echo
                     get_field('_sogo_header001_logo_img', 'options'); ?>"
                     alt="Logo">
            </a>
        </div>

        <div class="col-auto header001__menu-container">
            <div class="header001__menu d-flex h-100">
                <?php include 'header-walker-001.php'; ?>
                <?php include 'header-menu-001.php'; ?>
            </div>
        </div>

        <div class="col-auto hidden-md-down d-flex align-items-center">
            <div class="header001__jobs">
                <a class="button " href="<?php echo get_field
                ('_sogo_header001_button_link', 'options'); ?>">
                    <?php echo get_field('_sogo_header001_button', 'options') ?>
                </a>
            </div>
        </div>

        <div class="col-auto hidden-md-down d-flex align-items-center">
            <div class="header001__social-bar">
                <?php include "inc/header-social-bar-001.php"; ?>
            </div>
        </div>

    </div>

</header>


<style>

.nav__img {
    background-image: url("<?php echo get_field('_sogo_header001_sub_menu_image','options'); ?>");
}
</style>



