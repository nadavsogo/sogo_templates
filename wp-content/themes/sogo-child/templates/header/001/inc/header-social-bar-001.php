<div class="social-bar d-flex">
    <?php
    if (have_rows('_sogo_header001_socialbar','options')):
        while (have_rows('_sogo_header001_socialbar','options')) : the_row();
    ?>
        <a class="social-bar__a"
           href="<?php echo get_sub_field('link'); ?>">
            <?php echo sogo_file_get_contents(get_sub_field('image')); ?>
        </a>
    <?php
        endwhile;
    endif;
    ?>
</div>

