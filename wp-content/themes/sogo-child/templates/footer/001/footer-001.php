<footer>
    <div class="container-fluid bg-color4">
        <div class="row d-flex justify-content-center">
            <div class="col-10">
                <div class="row d-flex justify-content-between py-5">
                    <div class="col-lg mb-5 mb-lg-0 px-0">
                        <div class="contact-info">
                            <?php include "inc/logo.php"; ?>
                            <?php include 'inc/contact-details.php'; ?>
                            <?php include "inc/social-bar.php"; ?>
                        </div>
                    </div>

                    <div class="col-lg-6 mb-5 mb-lg-0 footer-menu">

                        <?php
                        //                        include 'walker.php';
                        wp_nav_menu(array(
                            'theme_location' => 'footer_menu',
                            'container' => false
                        ));
                        ?>
                    </div>

                    <div class="col-auto hidden-lg-down d-flex justify-content-center
                    justify-content-lg-start">
                        <div style="width: 260px">
                            <div class="fb-page"
                                 data-href="https://www.facebook.com/imdb"
                                 data-hide-cover="false"
                                 data-show-facepile="true"
                                 data-width="420"></div>
                            <?php ; ?> /div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
</footer>