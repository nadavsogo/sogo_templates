<div class="contact-details">
    <div>
        <?php echo sogo_file_get_contents(ROOT_PATH . '/images/icons/13_location-01.svg'); ?>
        <span><?php echo get_field( '_sogo_address', 'option' );?></span>
    </div>
    <div>
        <?php echo sogo_file_get_contents(ROOT_PATH . '/images/icons/14_paperplane-01.svg'); ?>
        <a href="mailto:info@bclass.co.il"><?php echo get_field( '_sogo_email', 'option' );?></a>
    </div>
    <div>
        <?php echo sogo_file_get_contents(ROOT_PATH . '/images/icons/15_phone-01.svg'); ?>
        <a href="tel:0773230051"><?php echo get_field( '_sogo_phone', 'option' );?></a>
    </div>
    <div>
        <?php echo sogo_file_get_contents(ROOT_PATH . '/images/icons/16_fax-01.svg'); ?>
        <span><?php echo get_field( '_sogo_fax', 'option' );?></span>
    </div>
</div>
<!-- contact-details -->


<!--<div>-->
<!--    --><?php //echo sogo_file_get_contents(ROOT_PATH . '/images/icons/13_location-01.svg'); ?>
<!--    <span>ז'בוטינקי 2, רמת גן, מגדל אטריום, קומה 8</span>-->
<!--</div>-->
<!--<div>-->
<!--    --><?php //echo sogo_file_get_contents(ROOT_PATH . '/images/icons/14_paperplane-01.svg'); ?>
<!--    <a href="mailto:info@bclass.co.il">info@bclass.co.il</a>-->
<!--</div>-->
<!--<div>-->
<!--    --><?php //echo sogo_file_get_contents(ROOT_PATH . '/images/icons/15_phone-01.svg'); ?>
<!--    <a href="tel:0773230051">077-3230051</a>-->
<!--</div>-->
<!--<div>-->
<!--    --><?php //echo sogo_file_get_contents(ROOT_PATH . '/images/icons/16_fax-01.svg'); ?>
<!--    <span>09-8666308</span>-->
<!--</div>-->
