<footer>
    <div class="container-fluid">
        <!--        <div class="row d-flex justify-content-center">-->
        <!--            <div class="col-10">-->
        <div class="row d-flex justify-content-center py-5 text-center">
            <div class="col-lg-2 mb-2 mb-lg-0 px-0">
                <div class="contact-info">
					<?php include "inc/footer-logo-002.php"; ?>
					<?php include 'inc/footer-contact-details-002.php'; ?>

					<?php if ( have_rows( '_sogo_footer002_social_bar', 'options' ) ) : ?>
						<?php include "inc/footer-social-bar-002.php"; ?>
					<?php endif; ?>
                </div>
            </div>

            <div class="col-lg-6 pt-lg-5 mb-lg-0 footer-menu">
				<?php include 'inc/footer-walker-002.php'; ?>
				<?php include 'inc/footer-menu-002.php'; ?>
            </div>
        </div>
        <!--                    <div class="col-auto hidden-lg-down d-flex justify-content-center-->
        <!--                    justify-content-lg-start">-->
        <!--                        <div style="width: 260px">-->
        <!--                            <div class="fb-page"-->
        <!--                                 data-href="https://www.facebook.com/imdb"-->
        <!--                                 data-hide-cover="false"-->
        <!--                                 data-show-facepile="true"-->
        <!--                                 data-width="420">-->
        <!--                            </div>-->
        <!--                            --><?php //; ?><!-- /div>-->
        <!--                        </div>-->
        <!--                    </div>-->


        <!--            </div>-->
        <!--        </div>-->
</footer>

