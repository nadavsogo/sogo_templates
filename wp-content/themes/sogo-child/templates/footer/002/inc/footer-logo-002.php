<a class="logo mb-3 text-lg-right" href="<?php echo esc_url(home_url('/')); ?>"
   title="<?php echo __('Go to: homepage', 'sogoc') ?>" rel="home">
    <img class="img-fluid logo-img"
         src="<?php echo get_field('_sogo_footer002_logo','options');?>"
         alt="<?php echo __('Logo','sogoc'); ?>">
</a>
