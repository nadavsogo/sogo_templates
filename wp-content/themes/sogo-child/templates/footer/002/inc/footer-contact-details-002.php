<div class="contact-details">
    <div class="d-flex flex-row justify-content-center text-lg-right align-items-start justify-content-lg-start mb-3">
        <div>
			<?php echo sogo_file_get_contents( ROOT_PATH . '/images/icons/15_location-01.svg' ); ?>

        </div>
        <span class="mr-3"><?php echo get_field( '_sogo_footer002_address', 'options' ); ?></span>
    </div>
    <div class="d-flex flex-row justify-content-center text-lg-right align-items-center justify-content-lg-start mb-3">
        <div>
			<?php echo sogo_file_get_contents( ROOT_PATH . '/images/icons/16_phone-01.svg' ); ?>

        </div>
        <a href="tel:<?php echo get_field( '_sogo_footer002_phone', 'options' ); ?>" class="mr-3">
			<?php echo get_field( '_sogo_footer002_phone', 'option' ); ?>
        </a>
    </div>
    <div class="d-flex flex-row justify-content-center text-lg-right align-items-center justify-content-lg-start mb-3">
        <div>
			<?php echo sogo_file_get_contents( ROOT_PATH . '/images/icons/19_fax-01.svg' ); ?>

        </div>
        <span class="mr-3"><?php echo get_field( '_sogo_footer002_fax', 'options' ); ?></span>
    </div>
    <div class="d-flex flex-row justify-content-center text-lg-right align-items-center justify-content-lg-start mb-3">
        <div>
			<?php echo sogo_file_get_contents( ROOT_PATH . '/images/icons/20_mail-01.svg' ); ?>

        </div>
        <a href="mailto:<?php echo get_field( '_sogo_footer002_email', 'option' ); ?>" class="mr-3">
			<?php echo get_field( '_sogo_footer002_email', 'option' ); ?>
        </a>
    </div>
</div>