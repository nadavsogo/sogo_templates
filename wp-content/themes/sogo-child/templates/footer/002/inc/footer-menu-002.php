<?php
wp_nav_menu( array(
'theme_location' => 'footer_menu',
'container'      => false,
'depth' => 2,
'walker'         => new Sogo_Footer_Menu()
) );
