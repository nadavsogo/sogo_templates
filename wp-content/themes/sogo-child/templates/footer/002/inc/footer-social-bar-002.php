<div class="social-bar d-flex justify-content-center justify-content-lg-start mt-lg-4">
	<?php while ( have_rows( '_sogo_footer002_social_bar', 'options' ) ) : the_row(); ?>
        <a class="social-bar__a"
           href="<?php echo get_sub_field( 'link' ); ?>">
            <?php echo sogo_file_get_contents( get_sub_field( 'image' ) ); ?>
        </a>
	<?php endwhile; ?>
</div>
