(function ($) {
    "use strict";
    $(document).ready(function () {

        //mobile menu
        $('.mobile-menu').click(function () {
            $('.nav__ul').addClass('nav__ul_show_true');
            $('.nav__exit-icon').addClass('nav__exit-icon_show_true');
        });


        $('.nav__exit-icon').click(function () {
            $('.nav__ul').removeClass('nav__ul_show_true');
            $('.nav__exit-icon').removeClass('nav__exit-icon_show_true');
        });

        //footer menus
        var screenWidth = screen.width;
        if (screenWidth < 720) {
            $('.menu > li').click(function () {
                $(this).find('a').attr('href', '#');
            })
        }

        //scroll to bottom
        $('.home-slider__arrow').on('click', function () {
            event.preventDefault();
            $('html, body').animate({
                scrollTop: $($.attr(this, 'href')).offset().top
            }, 500);
        });

        //animations
        if (screenWidth > 992) {
            $('.license__img-wrap').waypoint(function () {
                $('.license__img-wrap:first-of-type').addClass('animated fadeInRight');
                $('.license__img-wrap:last-of-type').addClass('animated fadeInLeftBig');
                $('.license__text').addClass('animated zoomIn');
            }, {offset: '65%'});

            // $('.projects__title').waypoint(function() {
            //     $('.projects__title').addClass('animated fadeInRight');
            // }, { offset: '100%' });

            // $('.attendants__title').waypoint(function() {
            //     $('.attendants__title').addClass('animated fadeInRight');
            // }, { offset: '65%' });

            $('.more-services__title').waypoint(function () {
                $('.more-services__title').addClass('animated fadeInRight');
            }, {offset: '65%'});
        }

        //sliders
        $('.testimonial-slider').slick({
            rtl: true,
            dots: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: false,
            infinite: true,
            arrows: true,
            nextArrow: '<i class="slick-next icon-right-dart"></i>',
            prevArrow: '<i class="slick-prev icon-left-dart"></i>',
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ],
            asNavFor: '.brand-slider'
        });

        $('.brand-slider').slick({
            rtl: true,
            dots: false,
            slidesToShow: 7,
            slidesToScroll: 4,
            autoplay: false,
            infinite: true,
            arrows: false,
            // nextArrow: '<i class="slick-next icon-right-dart"></i>',
            // prevArrow: '<i class="slick-prev icon-left-dart"></i>',
            responsive: [
                {
                    breakpoint: 769,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ],

            asNavFor: '.testimonial-slider',
            focusOnSelect: true,
            centerMode: true
        });

        $('.more-services__slider').slick({
            rtl: true,
            dots: true,
            slidesToShow: 2,
            slidesToScroll: 2,
            // autoplay: false,
            infinite: true,
            arrows: false,
            // nextArrow: '<i class="slick-next icon-right-dart"></i>',
            // prevArrow: '<i class="slick-prev icon-left-dart"></i>',
            responsive: [
                {
                    breakpoint: 1494,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
            // asNavFor: '.testimonial-slider',
            // focusOnSelect: true,
            // centerMode: true
        });

        $('.single-project__slider').slick({
            rtl: true,
            // dots: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            // autoplay: false,
            infinite: true,
            arrows: true,
            nextArrow: '<i class="slick-next single-project__left-arrow"></i>',
            prevArrow: '<i class="slick-prev single-project__right-arrow"></i>',
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
            // asNavFor: '.testimonial-slider',
            // focusOnSelect: true,
            // centerMode: true
        });
        $('.more-projects__slider').slick({
            rtl: true,
            dots: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            // autoplay: false,
            infinite: true,
            arrows: false,
            // nextArrow: '<i class="slick-next">></i>',
            // prevArrow: '<i class="slick-prev"><</i>',
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
            // asNavFor: '.testimonial-slider',
            // focusOnSelect: true,
            // centerMode: true
        });
        $('.single-project .more-project__slider').slick({
            rtl: true,
            dots: true,
            slidesToShow: 4,
            slidesToScroll: 4,
            // autoplay: false,
            infinite: true,
            arrows: false,
            // nextArrow: '<i class="slick-next">></i>',
            // prevArrow: '<i class="slick-prev"><</i>',
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
            // asNavFor: '.testimonial-slider',
            // focusOnSelect: true,
            // centerMode: true
        });

        $('.page-template-front-page .more-project__slider').slick({
            rtl: true,
            dots: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            // autoplay: false,
            // infinite: true,
            arrows: false
            // nextArrow: '<i class="slick-next">></i>',
            // prevArrow: '<i class="slick-prev"><</i>',
            // asNavFor: '.testimonial-slider',
            // focusOnSelect: true,
            // centerMode: true
        });


        $('.testimonials-video__slider').slick({
            rtl: true,
            dots: true,
            slidesToShow: 4,
            slidesToScroll: 1,
            // autoplay: false,
            infinite: true,
            arrows: false,
            // nextArrow: '<i class="slick-next">></i>',
            // prevArrow: '<i class="slick-prev"><</i>',
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
            // asNavFor: '.testimonial-slider',
            // focusOnSelect: true,
            // centerMode: true
        });

        $('.clients__slider').slick({
            rtl: true,
            // dots: true,
            slidesToShow: 7,
            slidesToScroll: 4,
            // autoplay: true,
            autoplay: true,
            autoplaySpeed: 3500,
            infinite: true,
            arrows: false,
            // nextArrow: '<i class="slick-next">></i>',
            // prevArrow: '<i class="slick-prev"><</i>',
            responsive: [
                {
                    breakpoint: 1201,
                    settings: {
                        slidesToShow: 5
                    }
                },
                {
                    breakpoint: 993,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3
                    }
                },
                {
                    breakpoint: 578,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 400,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
            // asNavFor: '.testimonial-slider',
            // focusOnSelect: true,
            // centerMode: true
        });


        /**
         * main slider
         */
        $('.main-carousel').slick({
            infinite: true,
            rtl: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false
        });

        /**
         * toggle class on footer menu in mobile
         */
        $(' .footer-menu .menu > li').click(function (e) {
            e.preventDefault();
            $(this).find('.sub-menu').toggleClass('active');
        })

        /**
         * chosen
         */
        function chosenMultiple(elem) {
            setTimeout(function(){
                elem.chosen( {
                    rtl: true
                });
            }, 1000);

        }

        chosenMultiple($('.chosen-multiple'));

    });

    //show form
    $('.page-jobs__bottom-banner-wrap a').click(function (event) {
        event.preventDefault();
        $('.job-form').addClass('form-show');
    });

    $('.page-jobs__job a').click(function (event) {
        // event.preventDefault();
        $('.job-form').addClass('form-show');
    });

    //scroll to top
    $('#move-up').click(function () {
        document.body.scrollTop = 0; // For Chrome, Safari and Opera
        document.documentElement.scrollTop = 0; // For IE and Firefox
    });
    window.onscroll = function () {
        scrollFunction()
    };

    function scrollFunction() {
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            $('#move-up').removeClass('hidden');
        } else {
            $('#move-up').addClass('hidden')
        }
    }

    /**
     *main-carousel scroll down
     */
    $('#main-carousel__down-arrow').click(function () {
        var heightToScroll = $(window).height() - $('header').height();
        $('html,body').animate({
            scrollTop: $(window).scrollTop() + heightToScroll
        }, 1000);
    });

    $('.page-template-page-jobs a[href*=#]:not([href=#])').click(function () {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html,body').animate({
                scrollTop: target.offset().top - $('header').height()
            }, 1000);
            return false;
        }
    });

    $('.inputfile').each(function () {
        var $input = $(this),
            $label = $input.next('label'),
            labelVal = $label.html();
        // console.log(labelVal);

        $input.on('change', function (e) {
            var fileName = '';
            if (this.files && this.files.length > 0) {
                fileName = ( this.getAttribute('data-caption') ).replace('{count}', this.files.length);

            } else if (e.target.value) {
                fileName = e.target.value.split('\\').pop();
            }
            if (fileName) {
                $label.find('.file-status').html(fileName);
            }
            else {
                $label.html(labelVal);
            }
        });
    });

    $('.job-form input[type="submit"]').click(function () {

        var label = $('.inputfile + label');
        label.after('<span style="color: red">זהו שדה חובה.</span>');
        label.css('border-color', 'red');

    });


    /**
     * datepicker
     */
    $(function () {
        $(".datepicker").datepicker();
    });


})(jQuery);


// $('.testimonial-slider').slick({
//     rtl: true,
//     dots: false,
//     slidesToShow: 1,
//     slidesToScroll: 1,
//     autoplay: false,
//     infinite: true,
//     arrows: true,
//     nextArrow: '<i class="slick-next icon-right-dart"></i>',
//     prevArrow: '<i class="slick-prev icon-left-dart"></i>',
//     responsive: [
//         {
//             breakpoint: 992,
//             settings: {
//                 slidesToShow: 1
//             }
//         }
//     ]
// });
//
// $('.brand-slider').slick({
//     rtl: true,
//     dots: false,
//     slidesToShow: 3,
//     slidesToScroll: 3,
//     autoplay: false,
//     infinite: true,
//     arrows: true,
//     nextArrow: '<i class="slick-next icon-right-dart"></i>',
//     prevArrow: '<i class="slick-prev icon-left-dart"></i>',
//     responsive: [
//         {
//             breakpoint: 992,
//             settings: {
//                 slidesToShow: 1
//             }
//         }
//     ]
// });


