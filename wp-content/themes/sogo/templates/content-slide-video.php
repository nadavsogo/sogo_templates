<div class="slide col-xs-12" style="background: url( {{bg}} ) center; background-size: cover;">
    <h2>{{slide.txt.title}}</h2>
    <h3>{{slide.txt.subtitle}}</h3>
    {{paragraph}}
    <a class="btn-main btn-slide" href="{{href}}" target="{{target}}">{{btnText}}</a>
</div>

